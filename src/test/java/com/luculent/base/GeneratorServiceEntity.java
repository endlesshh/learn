package com.luculent.base;

import org.junit.Test;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

public class GeneratorServiceEntity {



    @Test
    public void generateCode() {
        String packageName = "com.luculent.efficient.sys.privilege";
        boolean serviceNameStartWithI = false;//user -> UserService, 设置成true: user -> IUserService
        generateByTables(serviceNameStartWithI, packageName, "sys_privilege");
    }

    private void generateByTables(boolean serviceNameStartWithI, String packageName, String... tableNames) {
        GlobalConfig config = new GlobalConfig();
        String dbUrl = "jdbc:mysql://192.168.0.182:3306/efficient?useUnicode=true&useSSL=false&serverTimezone=GMT";
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setDbType(DbType.MYSQL)
                .setUrl(dbUrl)
                .setUsername("root")
                .setPassword("luculent")
                .setDriverName("com.mysql.cj.jdbc.Driver");
        StrategyConfig strategyConfig = new StrategyConfig();
        strategyConfig
//                .setCapitalMode(true)
//                .setEntityLombokModel(true)
//                .setSuperControllerClass(superControllerClass)
                .setNaming(NamingStrategy.underline_to_camel)
//                .setControllerMappingHyphenStyle(true)
                .setInclude(tableNames);//修改替换成你需要的表名，多个表名传数组
        
        config.setActiveRecord(false)
                .setAuthor("zhangyang")
                .setOutputDir("d:\\com")
                .setFileOverride(true)
                .setBaseResultMap(true)
                .setIdType(IdType.AUTO)
                .setEnableCache(false);
        if (!serviceNameStartWithI) {
            config.setServiceName("%sService");
        }
        config.setEntityName("%sDTO");
        config.setMapperName("%sMapper");
        config.setXmlName("%sDAO");
        new AutoGenerator().setGlobalConfig(config)
                .setDataSource(dataSourceConfig)
                .setStrategy(strategyConfig)
                .setPackageInfo(
                        new PackageConfig()
                                .setParent(packageName)
                                .setController("web")
                                .setEntity("dto")
                                .setXml("dao")
                                .setMapper("dao")
                ).execute();
    }

    private void generateByTables(String packageName, String... tableNames) {
        generateByTables(false, packageName, tableNames);
    }

}
