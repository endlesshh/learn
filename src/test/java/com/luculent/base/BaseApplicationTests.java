package com.luculent.base;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alibaba.fastjson.JSON;
import com.luculent.efficient.Application;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class BaseApplicationTests {
	
	public void debugJSON(Object obj) {
		System.err.println(JSON.toJSONString(obj, true));
	}

	@Test
	public void contextLoads() {
	}
}
