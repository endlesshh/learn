package com.luculent.dao;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.luculent.base.BaseApplicationTests;
import com.luculent.efficient.sys.privilege.dao.SysPrivilegeDAO;
import com.luculent.efficient.sys.privilege.dto.SysPrivilegeDTO;

public class SysPrivilegeDAOTest extends BaseApplicationTests{

	@Autowired
	private SysPrivilegeDAO sysPrivilegeDAO;
	
	/**
	 * 根据角色id查询对应的权限信息
	 * void
	 */
	@Test
	public void testSelectPrivilegeByRoleId() {
		List<SysPrivilegeDTO> list = sysPrivilegeDAO.selectPrivilegeByRoleId(1l);
		if(!CollectionUtils.isEmpty(list)) {
			debugJSON(list);
		}
	}

}
