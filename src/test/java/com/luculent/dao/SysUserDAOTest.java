package com.luculent.dao;


import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.luculent.base.BaseApplicationTests;
import com.luculent.efficient.sys.user.dao.SysUserDAO;
import com.luculent.efficient.sys.user.dto.SysUserDTO;

public class SysUserDAOTest extends BaseApplicationTests{

	@Autowired
	private SysUserDAO sysUserDAO;
	
	
	@Test
	public void testSelectUserWithRoleNameById() {
		SysUserDTO userDTO = sysUserDAO.selectUserWithRoleNameById(1001l);
		if(userDTO!=null) {
			debugJSON(userDTO);
		}
	}
	
	/**
	 * 通过sql日志可以看出已经查询出的一条数据，mybatis将这条数据映射到了两个类中，
	 * 像这种通过一次查询将结果映射到不同对象的方式，称之为关联的嵌套结果映射。
	 * void
	 */
	@Test
	public void testSelectUserAndRoleById() {
		//特别注意，在我们测试数据中，id = 1L 的用户有两个角色
		//由于后面覆盖前面的，因此只能得到最后一个角色
		//我们这里使用只有一个角色的用户（id = 1001L）
		//可以用 selectUserAndRoleById2 替换进行测试
		SysUserDTO userDTO = sysUserDAO.selectUserAndRoleById(1001l);
		if(userDTO!=null) {
			debugJSON(userDTO);
		}
	}
	
	@Test
	public void testSelectUserAndRoleById2() {
		//特别注意，在我们测试数据中，id = 1L 的用户有两个角色
		//由于后面覆盖前面的，因此只能得到最后一个角色
		//我们这里使用只有一个角色的用户（id = 1001L）
		//可以用 selectUserAndRoleById2 替换进行测试
		SysUserDTO userDTO = sysUserDAO.selectUserAndRoleById2(1001l);
		if(userDTO!=null) {
			debugJSON(userDTO);
		}
	}
	
	@Test
	public void testSelectUserAndRoleById3() {
		//特别注意，在我们测试数据中，id = 1L 的用户有两个角色
		//由于后面覆盖前面的，因此只能得到最后一个角色
		//我们这里使用只有一个角色的用户（id = 1001L）
		//可以用 selectUserAndRoleById3 替换进行测试
		SysUserDTO userDTO = sysUserDAO.selectUserAndRoleById3(1001l);
		if(userDTO!=null) {
			debugJSON(userDTO);
		}
	}
	
	
	/**
	 * 
	 * 结果和我们想的一致，因为第一个SQL 的查询结果只有一条，所以根据这一条数据的
		role id 关联了另一个查询，因此执行了两次SQL
	 */
	@Test
	public void testSelectUserAndRoleByIdSelect() {
		//特别注意，在我们测试数据中，id = 1L 的用户有两个角色
		//由于后面覆盖前面的，因此只能得到最后一个角色
		//我们这里使用只有一个角色的用户（id = 1001L）
		//可以用 selectUserAndRoleById3 替换进行测试
		SysUserDTO userDTO = sysUserDAO.selectUserAndRoleByIdSelect(1001l);
		if(userDTO!=null) {
			Assert.assertNotNull(userDTO);
			System.out.println("调用 user.getRole");
			Assert.assertNotNull(userDTO.getRole());
			debugJSON(userDTO);
		}
		
	}
	
	
	@Test
	public void testSelectByUserConstant() {
		SysUserDTO userDTO = sysUserDAO.selectByUserConstant();
		if(userDTO!=null) {
			debugJSON(userDTO);
		}
		
	}
	
	@Test
	public void testSelectByUserConstantMethod() {
		SysUserDTO userDTO = sysUserDAO.selectByUserConstantMethod();
		if(userDTO!=null) {
			debugJSON(userDTO);
		}
		
	}
	
	
	/**
	 * 查询所有用户及其对应的角色
	 * void
	 */
	@Test
	public void testSelectAllUserAndRoles() {
		List<SysUserDTO> list = sysUserDAO.selectAllUserAndRoles();
		if(!CollectionUtils.isEmpty(list)) {
			debugJSON(list);
		}
		
	}
	
	
	/**
	 * 查询所有用户及其对应的角色及权限 
	 * void
	 */
	@Test
	public void testSelectAllUserAndRoles1() {
		List<SysUserDTO> list = sysUserDAO.selectAllUserAndRoles1();
		if(!CollectionUtils.isEmpty(list)) {
			debugJSON(list);
		}
		
	}
	
	
	/**
	 * 查询所有用户包含其角色名称
	 * void
	 */
	@Test
	public void testSelectUserWithRoleName() {
		List<SysUserDTO> list = sysUserDAO.selectUserWithRoleName();
		if(!CollectionUtils.isEmpty(list)) {
			debugJSON(list);
		}
		
	}
	
	/**
	 * 嵌套查询所有用户及其对应的角色及权限
	 * void
	 */
	@Test
	public void testSelectAllUserAndRolesSelect() {
		List<SysUserDTO> list = sysUserDAO.selectAllUserAndRolesSelect(1l);
		if(!CollectionUtils.isEmpty(list)) {
			debugJSON(list);
		}
		
	}
	
	
	
	
	

}
