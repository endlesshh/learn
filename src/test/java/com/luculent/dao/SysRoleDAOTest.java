package com.luculent.dao;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.luculent.base.BaseApplicationTests;
import com.luculent.efficient.sys.role.dao.SysRoleDAO;
import com.luculent.efficient.sys.role.dto.SysRoleDTO;

public class SysRoleDAOTest extends BaseApplicationTests{

	@Autowired
	private SysRoleDAO sysRoleDAO;
	
	/**
	 * 查询所有角色及对应的权限
	 * void
	 */
	@Test
	public void testSelectAllRoleAndPrivileges() {
		List<SysRoleDTO> list = sysRoleDAO.selectAllRoleAndPrivileges();
		if(!CollectionUtils.isEmpty(list)) {
			debugJSON(list);
		}
	}
	
	/**
	 * 根据用户id查询对应的所有角色信息
	 * void
	 */
	@Test
	public void testSelectRoleByUserId() {
		List<SysRoleDTO> list = sysRoleDAO.selectRoleByUserId(1l);
		if(!CollectionUtils.isEmpty(list)) {
			debugJSON(list);
		}
	}
	
	
	/**
	 * 根据角色id查询对应的所有角色信息
	 * void
	 */
	@Test
	public void testSelectRoleByRoleIdChoose() {
		List<SysRoleDTO> enabledlist = sysRoleDAO.selectRoleByRoleIdChoose(1l);
		List<SysRoleDTO> noEnabledlist = sysRoleDAO.selectRoleByRoleIdChoose(2l);
		if(!CollectionUtils.isEmpty(enabledlist) && !CollectionUtils.isEmpty(noEnabledlist)) {
			System.err.println("=============角色状态为可用=================");
			debugJSON(enabledlist);
			System.err.println("=============角色状态为禁用=================");
			debugJSON(noEnabledlist);
		}
	}
	
	

}
