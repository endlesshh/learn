package luculent.learn;

public class Escape {
	private int thisCanBeEscape = 0;

	public Escape() throws InterruptedException{
		new InnerClass();
		this.thisCanBeEscape = 1;
	}

	private class InnerClass {
		public InnerClass() throws InterruptedException {
			// 这里可以在Escape对象完成构造前提前引用到Escape的private变量
			System.out.println(Escape.this.thisCanBeEscape);
		}
	}

	public static void main(String[] args)throws InterruptedException {
		new Escape();
	}
}