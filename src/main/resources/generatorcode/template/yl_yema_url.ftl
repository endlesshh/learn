package ${projectName}.danwei.${fullBussiName?lower_case}.api.yilaiymincrement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import ${projectName}.Commons;
import ${projectName}.danwei.${fullBussiName?lower_case}.${bussiName?upper_case}ApiCommand;
import ${projectName}.runable.UrlCommand;
import ${projectName}.runable.entity.Urls;
import ${projectName}.runable.entity.YiLaiSendUrls;

/**   
 * @Title: YiLaiYeMaUrlService
 * @Description: ${ftlDescription}
 * @author autoGenerator
 * @date ${ftlCreateTime}
 * @version V1.0   
 *
 */
@Service
public class ${bussiName?upper_case}YiLaiYeMaUrlService extends UrlCommand implements ${bussiName?upper_case}ApiCommand {

   @Autowired
    private JdbcTemplate jdbcTemplate;

    private String       sql    = "select name,apiname,pagenumbers,sendurl,batch,total from HTTP_YLPAGENUMBERS where state <> '" + Commons.psState
            + "' and  name='" + name + "'";
    private String       delSql = "delete from HTTP_YLSENDURLS where name='" + name + "'";

    @Override
    public JdbcTemplate getJdbc(){
        return this.jdbcTemplate;
    }

    @Override
    public String getSql(){
        return sql;
    }

    @Override
    public String getDelSlq(){
        return delSql;
    }

    @Override
    public String systemName(){
        return name;
    }

    @Override
    public Urls getUrls(){
        return new YiLaiSendUrls ();
    }
}
