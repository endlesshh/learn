package ${projectName}.danwei.${fullBussiName?lower_case}.api;

import luculent.fupin.danwei.${fullBussiName?lower_case}.${bussiName?upper_case}ApiCommand;
import luculent.fupin.runable.PageCommand;
import luculent.fupin.runable.PageData;
import luculent.pattern.visitor.login.NameAndPassword;


/**   
 * @Title: PageCommand
 * @Description: ${ftlDescription}
 * @author autoGenerator
 * @date ${ftlCreateTime}
 * @version V1.0   
 *
 */
public class ${bussiName?upper_case}PageCommand extends PageCommand implements ${bussiName?upper_case}ApiCommand{  
    
   public PageData data; 
     

    @Override
    public NameAndPassword getLogin(){
        return loginAcct;
    }

    @Override
    public String getSystemName(){
        return name;
    }  
    
}
