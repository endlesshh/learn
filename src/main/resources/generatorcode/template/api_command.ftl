package ${projectName}.danwei.${fullBussiName?lower_case};

import ${projectName}.param.MapStringParams;
import luculent.pattern.visitor.login.NameAndPassword;
import luculent.pattern.visitor.login.VisitorAccept;
/**   
 * @Title: ApiCommand
 * @Description: ${ftlDescription}
 * @author autoGenerator
 * @date ${ftlCreateTime}
 * @version V1.0   
 *
 */

public interface ${bussiName?upper_case}ApiCommand {
   
    String name = ${bussiName?upper_case}Commons.${fullBussiName?upper_case};
    // 保证登录正常
    String codeUrl                    = "";

    String userName                   = "";
    String password                   = "";

    String key                        = "iw-apikey=123";

    String url                        = "";
    
    public MapStringParams str = MapStringParams.builder().basePath(url + "?" + key).build();

    String login                      = "login";
 	String codeKey					  = "yzm";
 	
	public NameAndPassword loginAcct = VisitorAccept.builder().str(str).codeKey(codeKey).codeUrl(codeUrl).userName(userName).password(password).iwCmd(login).build();
    

    //下面是示例
    // 农户列表
    // String nonghuliebiao   = "nonghuliebiao";
    //人居环境列表
    //String  nongcunrenjuhuanjingxinxiliebiao2016 = "nongcunrenjuhuanjingxinxiliebiao2016";
    //危房试点农户列表
    //String weifangshidiannonghuliebiao = "weifangshidiannonghuliebiao";
    
    //农户详情
    //String nonghuxiangxixinxi = "nonghuxiangxixinxi";
    //人居环境列表详情
    //String renjuhuanjingxiangqing = "renjuhuanjingxiangqing";
    //危房试点农户档案详情
    //String weifangshidianxiangqingchaxun = "weifangshidianxiangqingchaxun";
    
}
