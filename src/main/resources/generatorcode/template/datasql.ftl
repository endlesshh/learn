package ${projectName}.danwei.${fullBussiName?lower_case}.api;

import ${projectName}.runable.UrlData;
import ${projectName}.runable.increment.GetInUrlData;
/**   
 * @Title: DataSql
 * @Description: ${ftlDescription}
 * @author autoGenerator
 * @date ${ftlCreateTime}
 * @version V1.0   
 *
 */
public abstract class ${bussiName?upper_case}DataSql extends ${bussiName?upper_case}DataCommand{
    public String niunsSql = " minus  (select ID,NAME,SENDURL,APINAME,BATCH from  HTTP_ERRORINSENDURLS)";
	@Override
    public UrlData getRunable(String username){ 
    	UrlData	data = new GetInUrlData (); 
        data.setJdbcTemplate (jdbcTemplate);
        data.setLogin (loginAcct);
        data.setRest (rest);
        data.setName (name);
        return data;
    }  
}
