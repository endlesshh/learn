package ${projectName}.danwei.${fullBussiName?lower_case};

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSONObject;
/**   
 * @Title: CommonalityParam
 * @Description: ${ftlDescription}
 * @author autoGenerator
 * @date ${ftlCreateTime}
 * @version V1.0   
 *
 */
@Service
public class ${bussiName?upper_case}CommonalityParam {

    private String            url   = "http://219.159.44.169:32201/upin/?iw-apikey=123&iw-cmd=xingzhengquhuazidian&ID=150400000000";
    private String            sql   = "select id from FPB_ADDRESS  where pid='150400000000'";
    @Autowired
    private RestTemplate      rest;
    @Autowired
    private JdbcTemplate      jdbcTemplate;
    Map<String, List<String>> xians = new HashMap<String, List<String>> ();

    // 查询的年度
    public Map<String, List<String>> getNianDu(){
        Map<String, List<String>> mapList = new HashMap<String, List<String>> ();
        List<String> a = new ArrayList<String> ();
        a.add ("2015");
        a.add ("2016");
        a.add ("2017");
        mapList.put ("ND", a);
        return mapList;
    } 
    
    public List<String> getPinSelect(String sql,Object[] params){
        List<Map<String, Object>> rows = jdbcTemplate.queryForList (sql, params);
        List<String> list = new ArrayList<String> ();
        for ( Map<String, Object> maps : rows ) {
            for ( Object key : maps.values () ) {
                list.add ((String) key + ",1");
            }
        }
        return list;
    }

    public List<String> getSelect(String sql,Object[] params){
        List<Map<String, Object>> rows = jdbcTemplate.queryForList (sql, params);
        List<String> list = new ArrayList<String> ();
        for ( Map<String, Object> maps : rows ) {
            for ( Object key : maps.values () ) {
                list.add ((String) key);
            }
        }
        return list;
    }

    public List<String> getSelect(String sql){
        List<Map<String, Object>> rows = jdbcTemplate.queryForList (sql);
        List<String> list = new ArrayList<String> ();
        for ( Map<String, Object> maps : rows ) {
            for ( Object key : maps.values () ) {
                list.add ((String) key);
            }
        }
        return list;
    }

}
