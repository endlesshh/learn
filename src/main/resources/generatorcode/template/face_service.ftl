package ${projectName}.danwei.${fullBussiName?lower_case};

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.jdbc.core.JdbcTemplate;

import ${projectName}.Commons;
import ${projectName}.danwei.${fullBussiName?lower_case}.api.yema.${bussiName?upper_case}YeMaInfo;
import ${projectName}.danwei.${fullBussiName?lower_case}.api.yema.${bussiName?upper_case}YeMaUrlService;
import ${projectName}.danwei.${fullBussiName?lower_case}.api.yema.${bussiName?upper_case}YeMaDataService;
import ${projectName}.danwei.${fullBussiName?lower_case}.api.yilaiymincrement.${bussiName?upper_case}YiLaiYeMaInfo;
import ${projectName}.danwei.${fullBussiName?lower_case}.api.yilaiymincrement.${bussiName?upper_case}YiLaiCleanYeMaInfo;
import ${projectName}.danwei.${fullBussiName?lower_case}.api.yilaiymincrement.${bussiName?upper_case}YiLaiYeMaUrlService;
import ${projectName}.danwei.${fullBussiName?lower_case}.api.yilaiymincrement.${bussiName?upper_case}YiLaiYeMaDataService;
import ${projectName}.danwei.${fullBussiName?lower_case}.api.yema.${bussiName?upper_case}CleanYeMaInfo;
import ${projectName}.danwei.${fullBussiName?lower_case}.api.increment.${bussiName?upper_case}Inexample;
import ${projectName}.danwei.${fullBussiName?lower_case}.api.createinurl.${bussiName?upper_case}Urlexample;
import luculent.pattern.visitor.login.NameAndPasswordVistior;
import luculent.pattern.visitor.login.visitor.LoginVisitor;
import ${projectName}.tools.DeleteTables;
import ${projectName}.runable.ControlThread;
import ${projectName}.runable.DataCommand;

/**   
 * @Title: ServiceFace
 * @Description: ${ftlDescription}
 * @author autoGenerator
 * @date ${ftlCreateTime}
 * @version V1.0   
 *
 */
@Service
public class ${bussiName?upper_case}ServiceFace extends ${bussiName?upper_case}Commons implements ${bussiName?upper_case}Face {

   	@Autowired
    JdbcTemplate jdbcTemplate; 
    // 基础分页数据
    @Autowired
    ${bussiName?upper_case}YeMaInfo                 createYm;
    
    @Autowired
    ${bussiName?upper_case}CleanYeMaInfo            cleanYm; 
    
    @Autowired
    ${bussiName?upper_case}YeMaUrlService           urls;
    @Autowired
    ${bussiName?upper_case}YeMaDataService          dataService;
    // 依赖基础数据的分页数据
    @Autowired
    ${bussiName?upper_case}YiLaiYeMaInfo            createYiLaiYm; 
    @Autowired
    ${bussiName?upper_case}YiLaiCleanYeMaInfo       cleanYiLaiYm;
    @Autowired
   	${bussiName?upper_case}YiLaiYeMaUrlService      yLaiUrls;
    @Autowired
    ${bussiName?upper_case}YiLaiYeMaDataService     yldataService;
    // 依赖基础数据的增量数据  
     @Autowired
    ${bussiName?upper_case}Inexample  increment;
     @Autowired
    ${bussiName?upper_case}Urlexample  createUrl;
     
    @Override
    public int createPageNumbersStepOne(){
        return ControlThread.pageCommand (${bussiName?lower_case}system,createYm,name,null);
    }
    
	@Override
    public int cleanPageNumbersStepTwo(){
        return ControlThread.cleanCommand (${bussiName?lower_case}system,cleanYm, name,null);
    }
    
	@Override
    public int createUrlStepThree(){
        return ControlThread.urlCommand (${bussiName?lower_case}system,urls, name);
    }
  

    @Override
    public int getBasicPageDataFour(int cutPoint){
        return ControlThread.dataCommand (${bussiName?lower_case}system,dataService, cutPoint, name, "GetBasicDataService",null);
    }

    @Override
    public int createIncrementFiveUrl(int cutPoint){
        Map<String, DataCommand> commands = new HashMap<String, DataCommand> (); 
        commands.put ("createUrl", createUrl);
        return ControlThread.dataCommands (${bussiName?lower_case}system,commands, cutPoint, name,null);
    }

    @Override
    public int createYiLaiIncrementSixData(int cutPoint){
        Map<String, DataCommand> commands = new HashMap<String, DataCommand> ();
        //commands.put ("increment", increment);
        return ControlThread.dataCommands (${bussiName?lower_case}system,commands, cutPoint, name,null);
    }
 

    @Override
    public int createYiLaiPageNumbersStepOne(){
        return ControlThread.pageCommand (${bussiName?lower_case}system,createYiLaiYm, name,null);
    }
    
    @Override
    public int cleanYiLaiPageNumbersStepTwo(){
        return ControlThread.cleanCommand (${bussiName?lower_case}system,cleanYiLaiYm, name,null);
    }
    
    @Override
    public int createYiLaiUrlStepThree(){
        return ControlThread.urlCommand (${bussiName?lower_case}system,yLaiUrls, name);
    }
    

    @Override
    public int getYiLaiBasicPageDataFour(int cutPoint){
        return ControlThread.dataCommand (${bussiName?lower_case}system,yldataService, cutPoint, name, "GetYiLaiYeMaDataService",null);
    }
    @Override
    public int getYiLaiIncrementFive(int cutPoint){
        Map<String, DataCommand> commands = new HashMap<String, DataCommand> (); 
       // commands.put ("beibangfurenliebiao", sBeibangfurenliebiao);  
        return ControlThread.dataCommands (${bussiName?lower_case}system,commands, cutPoint, name,null);
    }
    
    @Override
    public boolean login(int count){
        NameAndPasswordVistior login = LoginVisitor.getInstance();
    	boolean resutl = login.visitor(10,${bussiName?upper_case}ApiCommand.loginAcct);
        return resutl;
    }

    @Override
    public int getAll(int cutPoint){
        int state = ${bussiName?upper_case}Commons.${bussiName?lower_case}system.get (name);
        if (state == Commons.threadON) {
            ${bussiName?lower_case}system.put (name, Commons.threadTO);

            createYm.del ();
            createYm.execute (null);
            // 生成基础url
            urls.del ();
            urls.execute ();

            dataService.execute (cutPoint, name, "基础数据导入",null);

             
            
            increment.execute (cutPoint, name, "increment",null);
            
             
            createYiLaiYm.del ();
            createYiLaiYm.execute (null);
            yldataService.execute (cutPoint, name, "yldataService",null);
           
            ${bussiName?lower_case}system.put (name, Commons.threadON);
            return Commons.threadON;
        } else {
            return Commons.threadTO;
        }
    }
	@Override
    public int importAddress(){ 
        return 0;
    }
	
	@Override
	public boolean deleteTables() {
		try {
		//	jdbcTemplate.batchUpdate (DeleteTables.get${bussiName?upper_case}TableNames ());
		} catch (Exception e) {
			 e.printStackTrace();
		}
		
		return true;
	}
	
}
