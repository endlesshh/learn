package ${projectName}.danwei.${fullBussiName?lower_case};

import java.util.concurrent.ConcurrentHashMap;

import ${projectName}.Commons;

public class ${bussiName?upper_case}Commons {
    
    public static String                                      ${fullBussiName?upper_case}     = "${fullBussiName?upper_case}";
     
    String name  = ${fullBussiName?upper_case};
    
    public volatile static ConcurrentHashMap<String, Integer> ${bussiName?lower_case}logined=new ConcurrentHashMap<String,Integer>(){
          
        private static final long serialVersionUID = 2252250748560759228L;

        {put(${fullBussiName?upper_case},Commons.loginIn);}
    };
    
    public volatile static ConcurrentHashMap<String, Integer>  ${bussiName?lower_case}system       = new ConcurrentHashMap<String, Integer> () {
          
        private static final long serialVersionUID = 4308314114740714503L;

        {put (${fullBussiName?upper_case},Commons.threadON);}};
}
          


