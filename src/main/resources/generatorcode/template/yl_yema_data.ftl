package ${projectName}.danwei.${fullBussiName?lower_case}.api.yilaiymincrement;

import org.springframework.stereotype.Service;

import ${projectName}.Commons;
import ${projectName}.danwei.${fullBussiName?lower_case}.api.${bussiName?upper_case}DataCommand;
import ${projectName}.runable.UrlData;
import ${projectName}.runable.ylincrement.YiLaiGetYeMaUrlData;

/**   
 * @Title: YiLaiYeMaDataService
 * @Description: ${ftlDescription}
 * @author autoGenerator
 * @date ${ftlCreateTime}
 * @version V1.0   
 *
 */
@Service
public class ${bussiName?upper_case}YiLaiYeMaDataService extends ${bussiName?upper_case}DataCommand {

     
     private String        sql = "select id,name,apiname,sendurl,batch from HTTP_YLSENDURLS where state <>  '" + Commons.cState + "' and name='"
            + name + "'";

   
    @Override
    public String getSql(){
        return sql;
    }

    @Override
    public UrlData getRunable(String username){ 
    	UrlData data = new YiLaiGetYeMaUrlData ();
        data.setJdbcTemplate (jdbcTemplate);
        data.setLogin (loginAcct);
        data.setRest (rest);
        data.setName (name);
        return data;
    } 
}
