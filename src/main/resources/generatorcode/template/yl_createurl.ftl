package ${projectName}.danwei.${fullBussiName?lower_case}.api.createinurl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ${projectName}.danwei.${fullBussiName?lower_case}.${bussiName?upper_case}CommonalityParam;
import ${projectName}.danwei.${fullBussiName?lower_case}.${bussiName?upper_case}TableNameCommon;
import ${projectName}.danwei.${fullBussiName?lower_case}.api.${bussiName?upper_case}Execute;
import ${projectName}.param.MapListParams;

@Service
public class ${bussiName?upper_case}Urlexample extends ${bussiName?upper_case}Execute{

    @Autowired
    private ${bussiName?upper_case}CommonalityParam commonality;
    private String apiname="example"; 
    
    @Override
	public Map<String, String> getLine() {
		 Map<String, String> map = new HashMap<String, String> ();
	     map.put ("iw-cmd", apiname);
	     return map;
	}
    
    private String              sql = " ";
	@Override
	public MapListParams getLoop() {
		List<String> tiao = commonality.getSelect (sql);
        Map<String, List<String>> mapPin = new HashMap<String, List<String>> ();
        mapPin.put ("BFRID", tiao); 
        MapListParams mpList = new MapListParams (str,mapPin);
        return mpList;
	}

	@Override
	public String getApiname() { 
		return apiname;
	}
}

