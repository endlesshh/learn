package ${projectName}.danwei.${fullBussiName?lower_case}.api;

import java.util.List;
import java.util.Map;
import luculent.fupin.param.MapListParams;
import ${projectName}.Commons;
import ${projectName}.runable.entity.IncrementSendUrls;
/**   
 * @Title: DataCommand
 * @Description: ${ftlDescription}
 * @author autoGenerator
 * @date ${ftlCreateTime}
 * @version V1.0   
 *
 */
public abstract class ${bussiName?upper_case}Execute extends ${bussiName?upper_case}DataCommand{
    
	/*
	 * 获取串行的条件 必须存在
	 * 例如   xian=150402 cun=15040201 两个条件  单值
	 *  localhost:8080/new?xian='150402'&cun='15040201'
	 */
	public abstract Map<String,String> getLine();
	/*
	 * 获取并行的条件 可以不存在 	
	 * 例如   xian{150402,150403} 一个条件多个值
	 * 		localhost:8080/new?xian='150402'
	 * 	    localhost:8080/new?xian='150403'
	 * xian{150402,150403} cun{15040201,15040202}  两个条件有多值
	 * localhost:8080/new?xian='150402'&cun='15040201'
	 * localhost:8080/new?xian='150403'&cun='15040202'
	 * localhost:8080/new?xian='150402'&cun='15040201'
	 * localhost:8080/new?xian='150403'&cun='15040202'
	 */ 
	public abstract MapListParams getLoop();
	
	public abstract String getApiname();
	private  String   delSql = "delete from HTTP_INSENDURLS where apiname = ? and name = ?";
    
	@Override                                                                                                                                           
    public void execute(int cutPoint,String systemName,String apiName,String username){                                                                                                                         
		jdbcTemplate.update(delSql,getApiname(),name);
		List<String> urls = getUrls();
        System.out.println(getApiname()+":开始插入");
        for ( String url : urls ) {                                                                                                               
            IncrementSendUrls sul = new IncrementSendUrls ();                                                                                     
            sul.setSendurl (url);                                                                                                                 
            sul.setBatch (Commons.batch);                                                                                                         
            sul.setApiname(getApiname());                                                                                                              
            sul.setName(name);                                                                                                                    
            sul.setState(Commons.wState);  
            System.out.println(sul.toString());
            jdbcTemplate.update(sul.toString());
        }   
        urls.clear();
        System.out.println(getApiname()+":插入完成");
    } 
	
	//获取请求的连接
	public List<String> getUrls() { 
		Map<String, String> map = getLine(); 
        List<String> urls = null;
        if(getLoop()!=null){
        	urls = getLoop().prepare (map);
        }else{
        	urls = str.prepare (map);
        } 
        return urls;
	} 
}
