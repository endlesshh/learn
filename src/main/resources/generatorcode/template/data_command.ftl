package ${projectName}.danwei.${fullBussiName?lower_case}.api;

import java.util.List;

import ${projectName}.danwei.${fullBussiName?lower_case}.${bussiName?upper_case}ApiCommand;
import luculent.fupin.runable.DataCommand;
import luculent.fupin.runable.UrlData;
import luculent.fupin.runable.increment.GetUrlData;
import luculent.pattern.visitor.login.NameAndPassword;



/**   
 * @Title: DataCommand
 * @Description: ${ftlDescription}
 * @author autoGenerator
 * @date ${ftlCreateTime}
 * @version V1.0   
 *
 */
public class ${bussiName?upper_case}DataCommand extends DataCommand implements ${bussiName?upper_case}ApiCommand{
    
    public UrlData data;
  
    
    public NameAndPassword getLogin(){
        return loginAcct;
    } 

    @Override
    public UrlData getRunable(String username){ 
    	UrlData data = new GetUrlData ();
        data.setJdbcTemplate (jdbcTemplate); 
        data.setLogin (loginAcct);
        data.setRest (rest);
        data.setName (name);
        return data;
    } 
}
