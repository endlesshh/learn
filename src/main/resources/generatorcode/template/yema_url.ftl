package ${projectName}.danwei.${fullBussiName?lower_case}.api.yema;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import ${projectName}.Commons;
import ${projectName}.danwei.${fullBussiName?lower_case}.${bussiName?upper_case}ApiCommand;
import ${projectName}.runable.UrlCommand;
import ${projectName}.runable.entity.SendUrls;
import ${projectName}.runable.entity.Urls;

/**   
 * @Title: YeMaUrlService
 * @Description: ${ftlDescription}
 * @author autoGenerator
 * @date ${ftlCreateTime}
 * @version V1.0   
 *
 */
@Service
public class ${bussiName?upper_case}YeMaUrlService extends UrlCommand implements ${bussiName?upper_case}ApiCommand {

     private String       sql    = "select name,apiname,pagenumbers,sendurl,batch,total from HTTP_PAGENUMBERS where state <> '" + Commons.psState + "' and  name='"
            + name + "'";
    private String       delSql = "delete from HTTP_SENDURLS where name= '" + name + "'";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public JdbcTemplate getJdbc(){
        return this.jdbcTemplate;
    }

    @Override
    public String getSql(){
        return sql;
    }

    @Override
    public String getDelSlq(){
        return delSql;
    }

    @Override
    public String systemName(){
        return name;
    }

    @Override
    public Urls getUrls(){
        return new SendUrls ();
    }
}
