package ${projectName}.danwei.${fullBussiName?lower_case};
/**   
 * @Title: Face
 * @Description: ${ftlDescription}
 * @author autoGenerator
 * @date ${ftlCreateTime}
 * @version V1.0   
 *
 */
public interface ${bussiName?upper_case}Face {


	public boolean deleteTables(); 

    public boolean login(int count);

    public int importAddress();  
    
    public int createPageNumbersStepOne();
    
    public int cleanPageNumbersStepTwo(); 
    
    public int createUrlStepThree(); 
    
    public int getBasicPageDataFour(int cutPoint);  
     
    public int createIncrementFiveUrl(int cutPoint); 
    
    public int createYiLaiIncrementSixData(int cutPoint);
    
    
    
    public int createYiLaiPageNumbersStepOne();
    
    public int cleanYiLaiPageNumbersStepTwo();

    public int createYiLaiUrlStepThree();
    // 根据基础url生成基础数据
    public int getYiLaiBasicPageDataFour(int cutPoint);

    public int getYiLaiIncrementFive(int cutPoint); 
    

    public int getAll(int cutPoint);

}
