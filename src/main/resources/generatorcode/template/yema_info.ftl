package ${projectName}.danwei.${fullBussiName?lower_case}.api.yema;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import luculent.fupin.runable.entity.Page;
import ${projectName}.Commons;
import ${projectName}.danwei.${fullBussiName?lower_case}.${bussiName?upper_case}CommonalityParam;
import ${projectName}.danwei.${fullBussiName?lower_case}.api.${bussiName?upper_case}PageCommand;
import ${projectName}.param.MapListParams;

/**   
 * @Title: YeMaInfo
 * @Description: ${ftlDescription}
 * @author autoGenerator
 * @date ${ftlCreateTime}
 * @version V1.0   
 *
 */
@Service
public class ${bussiName?upper_case}YeMaInfo extends ${bussiName?upper_case}PageCommand  {

    private String              inSql  = "INSERT INTO HTTP_PAGENUMBERS (name,sendurl,apiname,total,pagenumbers,batch,state,time) VALUES (?,?,?,?,?,?,?,?)";
    private String              delSql = "delete from HTTP_PAGENUMBERS where name='" + name + "'";

    
    @Autowired
    private ${bussiName?upper_case}CommonalityParam commonality;
 
    
 	 
    @Override
    public List<Page> getSends(){
        List<Page> list = new ArrayList<Page>();
       // list.addAll (zirancunliebiao());
      //  list.addAll (pinkunhuxinxiliebiao());
       // list.addAll (xiangmuxinxichaxun());
      //  list.addAll (bangfurendanweichaxun()); 
       list.addAll (zcgzdbfdw()); 
        return list;
    } 

    
    public List<Page> zcgzdbfdw(){

        Map<String, String> map = new HashMap<String, String> ();
        //map.put ("iw-cmd", zcgzdbfdw); 
        MapListParams list1 = null;//new MapListParams (str,commonality.getQXQ ()); 
        List<String> urls = list1.prepare (map);
        return null;// getPage(urls,Commons.batch,zcgzdbfdw); 
    }
    
    

    @Override
    public String getInSql(){
        return inSql;
    }

    @Override
    public String getDelSql(){
        return delSql;
    }
}
