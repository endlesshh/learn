 
package ${projectName}.action;

import org.springframework.context.annotation.Scope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import ${projectName}.Commons;
import ${projectName}.danwei.${fullBussiName?lower_case}.${bussiName?upper_case}ServiceFace;
 
 
 
<#-- 列为文件类型的文件代码生成 -->
/**   
 * @Title: Controller
 * @Description: ${ftlDescription}
 * @author autoGenerator
 * @date ${ftlCreateTime}
 * @version V1.0   
 *
 */
@Scope("prototype")
@Controller
@RequestMapping("/${bussiName?lower_case}")
public class ${bussiName?upper_case}Controller {

    @Autowired
    ${bussiName?upper_case}ServiceFace face;

    
    @RequestMapping(params = "delete")
    @ResponseBody
    String delete(){
        boolean resutl = face.deleteTables();
        if (resutl) {
            return "删除成功";
        } else {
            return "删除失败";
        }
    }
    
    @RequestMapping(params = "login")
    @ResponseBody
    String login(){
        boolean resutl = face.login (10);
        if (resutl) {
            return "登录成功";
        } else {
            return "登录失败";
        }
    } 
    @RequestMapping(params = "one")
    @ResponseBody
    String createOne(){
        int resutl = face.createPageNumbersStepOne ();
        if (Commons.success == resutl) {
            return "生成全部链接完成";
        } else {
            return "进行中";
        }
    }
    @RequestMapping(params = "two")
    @ResponseBody
    String createTwo(){
        int resutl = face.cleanPageNumbersStepTwo ();
        if (Commons.success == resutl) {
            return "生成全部链接完成";
        } else {
            return "进行中";
        }
    }
    @RequestMapping(params = "three")
    @ResponseBody
    String createThree(){
        int resutl = face.createUrlStepThree ();
        if (Commons.success == resutl) {
            return "获取基础数据完成";
        } else {
            return "获取基础数据完成进行中";
        }
    } 
    
    @RequestMapping(params = "four")
    @ResponseBody
    String createFour(){
        int resutl = face.getBasicPageDataFour (1000);
        if (Commons.success == resutl) {
            return "导入增量数据完成完成";
        } else {
            return "导入增量数据进行中";
        }
    }
	  
    @RequestMapping(params = "five")
    @ResponseBody
    String createFive(){
        int resutl = face.createIncrementFiveUrl (1000);
        if (Commons.success == resutl) {
            return "导入增量数据完成完成";
        } else {
            return "导入增量数据进行中";
        }
    }
    
    
    @RequestMapping(params = "six")
    @ResponseBody
    String createSix(){
        int resutl = face.createYiLaiIncrementSixData (1000);
        if (Commons.success == resutl) {
            return "导入增量数据完成完成";
        } else {
            return "导入增量数据进行中";
        }
    }
    
    @RequestMapping(params = "yone")
    @ResponseBody
    String createSeven(){
        int resutl = face.createYiLaiPageNumbersStepOne ();
        if (Commons.success == resutl) {
            return "依赖增量数据生成完毕";
        } else {
            return "依赖增量数据进行中";
        }
    }
    @RequestMapping(params = "ytwo")
    @ResponseBody
    String createEight (){
        int resutl = face.cleanYiLaiPageNumbersStepTwo  ();
        if (Commons.success == resutl) {
            return "依赖增量数据生成完毕";
        } else {
            return "依赖增量数据进行中";
        }
    }
    @RequestMapping(params = "ythree")
    @ResponseBody
    String createNine (){
        int resutl = face.createYiLaiUrlStepThree ();
        if (Commons.success == resutl) {
            return "依赖增量数据生成完毕";
        } else {
            return "依赖增量数据进行中";
        }
    }
    @RequestMapping(params = "yfour")
    @ResponseBody
    String createTen (){
        int resutl = face.getYiLaiBasicPageDataFour (1000);
        if (Commons.success == resutl) {
            return "依赖增量数据生成完毕";
        } else {
            return "依赖增量数据进行中";
        }
    } 
     
    @RequestMapping(params = "yfive")
    @ResponseBody
    String createEleven(){
        int resutl = face.getYiLaiIncrementFive (1000);
        if (Commons.success == resutl) {
            return "依赖增量数据生成完毕";
        } else {
            return "依赖增量数据生成进行中";
        }
    }
    
    
    
    
    @RequestMapping(params = "all")
    @ResponseBody
    String all(){
        int resutl = face.getAll (10000);
        if (Commons.success == resutl) {
            return "全部数据导入完毕";
        } else {
            return "数据导入中";
        }
    }
}
 