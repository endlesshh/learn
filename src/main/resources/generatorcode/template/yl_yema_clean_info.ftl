package luculent.fupin.danwei.${fullBussiName?lower_case}.api.yilaiymincrement;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import luculent.fupin.Commons;
import luculent.fupin.danwei.${fullBussiName?lower_case}.api.${bussiName?upper_case}PageCommand;
import luculent.fupin.runable.PageData;
import luculent.fupin.runable.entity.Page;
import luculent.fupin.runable.entity.PageNumbers;
import luculent.fupin.runable.yema.UpPageUrl;

/**
 * 页码数据清理
 * @author ShiQiang
 *
 */
@Service
public class ${bussiName?upper_case}YiLaiCleanYeMaInfo extends ${bussiName?upper_case}PageCommand {

    // 将状态是 0和2的进行重新请求
    private String        sendAgainSql  = "select name,apiname,pagenumbers,sendurl,batch,total from HTTP_YLPAGENUMBERS  where name='" + name
            + "' and  state <> '" + Commons.pcState + "'";
    // 请求成功更新为2
    private String        inSql         = "UPDATE HTTP_YLPAGENUMBERS SET total = ? ,pagenumbers = ?,state = '" + Commons.cState + "' where sendurl = ?";
  
    private String              delSql = "delete from HTTP_YLPAGENUMBERS where name='" + name + "' and  sendurl = ?";

    
    @Override
    public String getDelSql(){
        return delSql;
    } 
    @Override
    public String getInSql(){
        return inSql;
    } 
    
    @Override
    public PageData getRunable(String username){
        PageData data = new UpPageUrl ();
        data.setJdbcTemplate (super.jdbcTemplate);
        data.setLogin (loginAcct);
        data.setRest (super.rest);
        data.setName (name);
        data.setDelSql(delSql);
        return data;
    }
     
    @Override
    public List<Page> getSends(){
        List<PageNumbers> yemaxinxi = super.jdbcTemplate.query (sendAgainSql, new PageNumbers ());
         
        List<Page> pages = new ArrayList<Page>();
        for(PageNumbers pagenums : yemaxinxi){
            Page page = new Page();
            page.setUrl (pagenums.getSendurl ());
            page.setBatch (Commons.batch); 
            pages.add (page);
        }
        return pages;  
    }  
}
