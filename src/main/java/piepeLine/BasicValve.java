package piepeLine;

public class BasicValve implements Valve {
 
    protected Valve next;
    public Valve getNext() {
        return next;
    }
 
    public void setNext(Valve valve) {
        next = valve;
    }
 
    public void invoke(String s) {
        s = s.replace("a","b");
        System.out.println("after basic valve handled s = " + s);
    }
} 