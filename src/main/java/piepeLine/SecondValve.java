package piepeLine;
public class SecondValve implements Valve {
    public Valve next;
    public Valve getNext() {
        return next;
    }
 
    public void setNext(Valve v) {
        next = v;
    }
 
    public void invoke(String s) {
        s = s.replace("1","2");
        System.out.println("after second Valve handled: s = " + s);
        //注意这行代码
        getNext().invoke(s);
    }
}
 