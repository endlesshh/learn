package piepeLine;
public class ThirdValve implements Valve {
    public Valve next;
    public Valve getNext() {
        return next;
    }
 
    public void setNext(Valve v) {
        next = v;
    }
 
    public void invoke(String s) {
        s = s.replace("a","e");
        System.out.println("after third Valve handled: s = " + s);
        //注意这行代码
        getNext().invoke(s);
    }
}
 