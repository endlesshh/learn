package piepeLine;
public interface Pipeline {
    public Valve getFirst();
    public Valve getBasic();
    public void setBasic(Valve v);
    public void addValve(Valve v);
}  