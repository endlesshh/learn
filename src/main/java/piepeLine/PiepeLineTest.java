package piepeLine;

public class PiepeLineTest {
	public static void main(String[] args) {
		String s = "00,11,11,aa,bb";
	    StandardPipeline pipeline = new StandardPipeline();
	    BasicValve basic = new BasicValve();
	    SecondValve second = new SecondValve();
	    ThirdValve third = new ThirdValve();
	    ThirdValve third1 = new ThirdValve();
	    pipeline.setBasic(basic);
	    pipeline.addValve(second);
	    pipeline.addValve(third);
	    pipeline.addValve(third1);
	    pipeline.getFirst().invoke(s); 
	}
}
