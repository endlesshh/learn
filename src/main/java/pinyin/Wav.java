package pinyin;
import java.io.File;
import java.io.SequenceInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

import com.github.stuxuhai.jpinyin.PinyinFormat;
import com.github.stuxuhai.jpinyin.PinyinHelper;
import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.seg.common.Term;

import cn.hutool.core.collection.CollectionUtil;

public class Wav {
	 
	public static void main(String[] args) throws Exception {
		//生成的新.wav路径
		File fileOut = new File("F:/face/1.wav");
		String str = "夜结束了一天的喧嚣后安静下来伴随着远处路灯那微弱的光风毫无预兆地席卷整片旷野撩动人的思绪万千星遥遥地挂在天空之中闪烁着它那微微星光不如阳光般灿烂却如花儿般如痴如醉"; 
	   
	    System.out.println(HanLP.segment(str));
	    List<Term> pinyinList = HanLP.segment(str);
	    AudioInputStream audioBuild = null;
	    for (Term pinyin : pinyinList){
            String pin = pinyin.word;
            String conver = PinyinHelper.convertToPinyinString(pin, ",", PinyinFormat.WITH_TONE_NUMBER).replaceAll("5", "");// ni3,hao3,shi4,jie4
    	    System.out.println(conver);
    	    String[] wavP = conver.split(",");
    	    List<String> listArr = new ArrayList<String>();
    	    for(String na : wavP){
    	    	listArr.add("E:/tools/syllables/syllables/"+na+".wav");
    	    }
    	    audioBuild = conbin(audioBuild,listArr);
        } 
	    //生成语音
	    AudioFormat sourceFormat =  audioBuild.getFormat();
	    System.out.println(sourceFormat.getSampleSizeInBits());
	    System.out.println(sourceFormat.getSampleSizeInBits());
	    AudioFormat targetFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
	    		44100f,
                sourceFormat.getSampleSizeInBits(),
                sourceFormat.getChannels(),
                sourceFormat.getFrameSize(),
                sourceFormat.getFrameRate(),
                sourceFormat.isBigEndian());

        AudioInputStream inputStream = AudioSystem.getAudioInputStream(targetFormat, audioBuild);
		AudioSystem.write(inputStream, AudioFileFormat.Type.WAVE, fileOut );
	   
	}
	
	private static AudioInputStream conbin(AudioInputStream audioBuild,List<String> listArr) throws Exception{
		if(CollectionUtil.isEmpty(listArr)){
			return null;
		}
		AudioInputStream audioOne = AudioSystem.getAudioInputStream(new File(listArr.get(0)));
		int total = listArr.size();
		AudioInputStream audioTwo;
		for(int i = 1; i<total;i++){
			audioTwo = AudioSystem.getAudioInputStream(new File(listArr.get(i)));
			audioOne = new AudioInputStream(
		            new SequenceInputStream(audioOne, audioTwo),
		            audioOne.getFormat(), audioOne.getFrameLength() + audioTwo.getFrameLength() );
		} 
		
		if(audioBuild == null){
			audioBuild = audioOne;
		}else{
			audioBuild = new AudioInputStream(
		            new SequenceInputStream(audioBuild, audioOne),
		            audioBuild.getFormat(), audioBuild.getFrameLength() +
		            (audioOne.getFrameLength()));
		}
		return audioBuild; 
	}
	
}

