package pinyin;

import java.io.File;

import ws.schild.jave.AudioAttributes;
import ws.schild.jave.Encoder;
import ws.schild.jave.EncodingAttributes;
import ws.schild.jave.MultimediaObject;

public class Jave {
	public static void main(String[] args) throws Exception {
		File source = new File("F:/face/1.wav");
		File target = new File("F:/face/target.wav");
		AudioAttributes audio = new AudioAttributes();
		audio.setCodec("pcm_s16le");
		audio.setBitRate(new Integer(128000)); //128000
		audio.setChannels(new Integer(2));
		audio.setSamplingRate(new Integer(44100)); //44100 
		EncodingAttributes attrs = new EncodingAttributes();
		attrs.setFormat("wav");
		attrs.setAudioAttributes(audio); 
	
		Encoder encoder = new Encoder();
		encoder.encode(new MultimediaObject(source), target, attrs);
	}
}
