package pinyin;

import java.util.List;

import com.github.stuxuhai.jpinyin.PinyinFormat;
import com.github.stuxuhai.jpinyin.PinyinHelper;
import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.dictionary.py.Pinyin;

public class Cn {
	public static void main(String[] args) {
		String str = "基于词典和贝叶斯模型的情感分析主程序";
	    System.out.println(PinyinHelper.convertToPinyinString(str, ",", PinyinFormat.WITH_TONE_MARK)); // nǐ,hǎo,shì,jiè
	    System.out.println(PinyinHelper.convertToPinyinString(str, ",", PinyinFormat.WITH_TONE_NUMBER));// ni3,hao3,shi4,jie4
	    System.out.println(PinyinHelper.convertToPinyinString(str, ",", PinyinFormat.WITHOUT_TONE)); // ni,hao,shi,jie
	    System.out.println(PinyinHelper.getShortPinyin(str)); // nhsj
	    System.out.println(HanLP.segment(str));
	    List<Pinyin> pinyinList = HanLP.convertToPinyinList(str);
	    System.out.print("拼音（数字音调）,");
        for (Pinyin pinyin : pinyinList)
        {
            System.out.printf("%s,", pinyin);
        }
	}
}
