package pinyin;

import java.util.List;

import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.dictionary.py.Pinyin;

public class CnPin {
	public static void main(String[] args) {
		String text = "重载不是重任";
		List<Pinyin> pinyinList = HanLP.convertToPinyinList(text);
		for(Pinyin p : pinyinList){
			System.out.println(p.getPinyinWithoutTone());
		}

	}
}
