package luculent.μJavaActors.mytest.normal;

import luculent.μJavaActors.AbstractActor;
import luculent.μJavaActors.DefaultMessage;
import luculent.μJavaActors.test.DefaultActorTest;

/**
 * An Actor superclass that provided access to a runtime helper. 
 * 
 * @author BFEIGENB
 *
 */
abstract public class TestableActor2 extends AbstractActor {
	DefaultActorTest2 actorTest;

	public DefaultActorTest2 getActorTest() {
		return actorTest;
	}

	public void setActorTest(DefaultActorTest2 actorTest) {
		this.actorTest = actorTest;
	}

	public TestableActor2() {
	}

}
