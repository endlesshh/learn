package luculent.μJavaActors.mytest.mapreduce;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import luculent.μJavaActors.Actor;
import luculent.μJavaActors.DefaultActorManager;
import luculent.μJavaActors.DefaultMessage;
import luculent.μJavaActors.logging.DefaultLogger;
import luculent.μJavaActors.test.MapReduceActor;
import luculent.μJavaActors.test.SumOfSquaresReducer;
import luculent.μJavaActors.utils.Utils;

/** 
 * A set of runtime services for testing actors and a test case driver. 
 * 
 * @author BFEIGENB
 *
 */
public class DefaultActorTest extends Utils {

	public static final int MAX_IDLE_SECONDS = 10;

	// public static final int STEP_COUNT = 3 * 60;
	public static final int TEST_VALUE_COUNT = 1000; // TODO: make bigger

	public DefaultActorTest() {
		super();
	}

	private Map<String, Actor> testActors = new ConcurrentHashMap<String, Actor>();

	static Random rand = new Random();

	public static int nextInt(int limit) {
		return rand.nextInt(limit);
	}

	protected DefaultActorManager getManager() {
		DefaultActorManager am = actorManager != null ? actorManager : new DefaultActorManager();
		return am;
	}

	protected int stepCount = 120;

	public void setStepCount(int stepCount) {
		this.stepCount = stepCount;
	}

	public int getStepCount() {
		return stepCount;
	}

	protected int threadCount = 10;

	public int getThreadCount() {
		return threadCount;
	}

	public void setThreadCount(int threadCount) {
		this.threadCount = threadCount;
	}

	public void setTestActors(Map<String, Actor> testActors) {
		this.testActors = testActors;
	}

	public Map<String, Actor> getTestActors() {
		return testActors;
	}

	public static final int COMMON_ACTOR_COUNT = 10;
	public static final int TEST_ACTOR_COUNT = 25;
	public static final int PRODUCER_ACTOR_COUNT = 25;

	public static void sleeper(int seconds) {
		int millis = seconds * 1000 + -50 + nextInt(100); // a little
															// variation
		// logger.trace("sleep: %dms", millis);
		sleep(millis);
	}

	public static void dumpMessages(List<DefaultMessage> messages) {
		synchronized (messages) {
			if (messages.size() > 0) {
				for (DefaultMessage m : messages) {
					logger.info("%s", m);
				}
			}
		}
	}

	protected List<ChangeListener> listeners = new LinkedList<ChangeListener>();

	public void addChangeListener(ChangeListener l) {
		if (!listeners.contains(l)) {
			listeners.add(l);
		}
	}

	public void removeChangeListener(ChangeListener l) {
		listeners.remove(l);
	}

	protected void fireChangeListeners(ChangeEvent e) {
		for (ChangeListener l : listeners) {
			l.stateChanged(e);
		}
	} 

	public static void main(String[] args) {
		DefaultActorTest at = new DefaultActorTest();
		at.run(args);
		logger.trace("Done");
	}

	protected String title;

	public String getTitle() {
		return title;
	}

	 

	volatile protected boolean done;

	public void terminateRun() {
		done = true;
	}
 

	DefaultActorManager actorManager;

	public DefaultActorManager getActorManager() {
		return actorManager;
	}

	public void setActorManager(DefaultActorManager actorManager) {
		this.actorManager = actorManager;
	}

	public void run(String[] args) {
		done = false;
		// DefaultLogger.getDefaultInstance().setIncludeDate(false);
		DefaultLogger.getDefaultInstance().setIncludeContext(false);
		DefaultLogger.getDefaultInstance().setIncludeCaller(false);
		// DefaultLogger.getDefaultInstance().setIncludeThread(false);
		DefaultLogger.getDefaultInstance().setLogToFile(false);
		DefaultLogger.getDefaultInstance().setThreadFieldWidth(10);

		int sc = stepCount;
		int tc = threadCount;
		boolean doMapReduce = true;
		title = ""; 
		if (doMapReduce) {
			if (title.length() > 0) {
				title += " ";
			}
			title += "(MapReduce)";
		}
		 
		DefaultActorManager am = getManager();
		try {
			Map<String, Object> options = new HashMap<String, Object>();
			options.put(DefaultActorManager.ACTOR_THREAD_COUNT, tc);
			am.initialize(options); 

			 
			if (doMapReduce) {
				BigInteger[] values = new BigInteger[TEST_VALUE_COUNT];
				for (int i = 0; i < values.length; i++) {
					values[i] = new BigInteger(Long.toString((long) rand.nextInt(values.length)));
				}
				BigInteger[] targets = new BigInteger[Math.max(1, values.length / 10)];

				BigInteger res = new BigInteger("0");
				for (int i = 0; i < values.length; i++) {
					res = res.add(values[i].multiply(values[i]));
				}

				String id = MapReduceActor.nextId();
				logger.trace("**** MapReduce %s (expected=%d) start: %s", id, res, values);

				// start at least 5 actors
				MapReduceActor.createMapReduceActor(am, 10);
				MapReduceActor.createMapReduceActor(am, 10);
				MapReduceActor.createMapReduceActor(am, 10);
				MapReduceActor.createMapReduceActor(am, 10);
				MapReduceActor.createMapReduceActor(am, 10);
				// getTestActors().put(mra.getName(), mra);

				DefaultMessage dm = new DefaultMessage("init", new Object[] { values, targets,
						SumOfSquaresReducer.class });
				am.send(dm, null, MapReduceActor.getCategoryName());
			}
  
			
			for (int i = sc; i > 0; i--) {
				if (done) {
					break;
				}
				// see if idle a while
				long now = new Date().getTime();
				if (am.getActiveRunnableCount() == 0) {
					if (now - am.getLastDispatchTime() > MAX_IDLE_SECONDS * 1000
							&& now - am.getLastSendTime() > MAX_IDLE_SECONDS * 1000) {
						break;
					}
				}
				setStepCount(i);
				fireChangeListeners(new ChangeEvent(this));
				if (i < 10 || i % 10 == 0) {
					logger.trace("main waiting: %d...", i);
				}
				sleeper(1);
			}
			setStepCount(0);
			fireChangeListeners(new ChangeEvent(this));

			// logger.trace("main terminating");
			am.terminateAndWait();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
