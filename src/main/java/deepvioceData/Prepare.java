package deepvioceData;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IORuntimeException;

public class Prepare {
	static String dataPath = "D:/dataset/qinghua/";
	static String encode = "UTF-8";
	public static void main(String[] args) throws IORuntimeException, IOException {
		List<File> files = FileUtil.loopFiles("F:/voice/data_thchs30/data1");
		File alignment = new File(dataPath+"/alignment.json");
		PrintWriter wirter = FileUtil.getPrintWriter(alignment, encode, true);
		wirter.append("{");
		for(int i=1,total=files.size();i<total;i++){
			File file = files.get(i-1);
			if(file.getName().endsWith(".trn")){ 
				BufferedReader reader = FileUtil.getReader(file, encode);
				reader.readLine();
				wirter.append("\""+dataPath+"audio/"+i/2+".wav\" : \"");
				wirter.append(reader.readLine());
				wirter.append("\",\n");
			}else if(file.getName().endsWith(".wav")){
				FileUtil.copy(file, new File(dataPath+"audio/"+i/2+".wav"), false);
			}
		} 
		wirter.append("}");
		wirter.close();
	}
}
