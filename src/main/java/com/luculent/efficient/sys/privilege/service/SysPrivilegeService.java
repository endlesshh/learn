package com.luculent.efficient.sys.privilege.service;

import com.luculent.efficient.sys.privilege.dto.SysPrivilegeDTO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author zhangyang
 * @since 2019-04-30
 */
public interface SysPrivilegeService extends IService<SysPrivilegeDTO> {

}
