package com.luculent.efficient.sys.privilege.service.impl;

import com.luculent.efficient.sys.privilege.dto.SysPrivilegeDTO;
import com.luculent.efficient.sys.privilege.dao.SysPrivilegeDAO;
import com.luculent.efficient.sys.privilege.service.SysPrivilegeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author zhangyang
 * @since 2019-04-30
 */
@Service
public class SysPrivilegeServiceImpl extends ServiceImpl<SysPrivilegeDAO, SysPrivilegeDTO> implements SysPrivilegeService {

}
