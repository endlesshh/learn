package com.luculent.efficient.sys.privilege.dao;

import com.luculent.efficient.sys.privilege.dto.SysPrivilegeDTO;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author zhangyang
 * @since 2019-04-30
 */
public interface SysPrivilegeDAO extends BaseMapper<SysPrivilegeDTO> {

	/**
	 * 根据角色id查询对应的权限信息
	 *@param roleId
	 *@return List<SysPrivilegeDTO>
	 */
	List<SysPrivilegeDTO> selectPrivilegeByRoleId(Long roleId);
}
