package com.luculent.efficient.sys.role.service.impl;

import com.luculent.efficient.sys.role.dto.SysRoleDTO;
import com.luculent.efficient.sys.role.dao.SysRoleDAO;
import com.luculent.efficient.sys.role.service.SysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author zhangyang
 * @since 2019-04-30
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleDAO, SysRoleDTO> implements SysRoleService {

}
