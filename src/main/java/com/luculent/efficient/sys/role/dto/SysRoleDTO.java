package com.luculent.efficient.sys.role.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.luculent.efficient.sys.privilege.dto.SysPrivilegeDTO;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.util.List;
import java.io.Serializable;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author zhangyang
 * @since 2019-04-30
 */
@TableName("sys_role")
public class SysRoleDTO implements Serializable {

private static final long serialVersionUID=1L;

    /**
     * 角色ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 角色名
     */
    private String roleName;

    /**
     * 有效标志
     */
    private Integer enabled;

    /**
     * 创建人
     */
    private Long createBy;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    
    
    /**
     * 权限集合
     */
    @TableField(exist=false)
    private List<SysPrivilegeDTO> privilegeList;
    


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }
    
    

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    
    

	public List<SysPrivilegeDTO> getPrivilegeList() {
		return privilegeList;
	}

	public void setPrivilegeList(List<SysPrivilegeDTO> privilegeList) {
		this.privilegeList = privilegeList;
	}

}
