package com.luculent.efficient.sys.role.dao;

import com.luculent.efficient.sys.role.dto.SysRoleDTO;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author zhangyang
 * @since 2019-04-30
 */
public interface SysRoleDAO extends BaseMapper<SysRoleDTO> {

	/**
	 * 查询所有角色及对应的权限
	 *@return List<SysRoleDTO>
	 */
	List<SysRoleDTO> selectAllRoleAndPrivileges();
	
	/**
	 * 根据用户id查询对应的所有角色信息
	 *@return List<SysRoleDTO>
	 */
	List<SysRoleDTO> selectRoleByUserId(Long userId);
	

	/**
	 * 根据角色id查询对应的所有角色信息
	 *@param roleId
	 *@return List<SysRoleDTO>
	 */
	List<SysRoleDTO> selectRoleByRoleIdChoose(Long roleId);
}
