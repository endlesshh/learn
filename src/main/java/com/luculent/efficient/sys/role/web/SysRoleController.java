package com.luculent.efficient.sys.role.web;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author zhangyang
 * @since 2019-04-30
 */
@Controller
@RequestMapping("/sysRoleDTO")
public class SysRoleController {

}

