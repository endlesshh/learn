package com.luculent.efficient.sys.role.service;

import com.luculent.efficient.sys.role.dto.SysRoleDTO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author zhangyang
 * @since 2019-04-30
 */
public interface SysRoleService extends IService<SysRoleDTO> {

}
