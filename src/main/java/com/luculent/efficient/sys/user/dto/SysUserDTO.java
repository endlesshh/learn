package com.luculent.efficient.sys.user.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.luculent.efficient.sys.role.dto.SysRoleDTO;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author zhangyang
 * @since 2019-04-30
 */
@TableName("sys_user")
public class SysUserDTO implements Serializable {

private static final long serialVersionUID=1L;

    /**
     * 用户ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String userPassword;

    /**
     * 邮箱
     */
    private String userEmail;

    /**
     * 简介
     */
    private String userInfo;

    /**
     * 头像
     */
    private String headImg;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    
    
    /**
     * 角色名称
     */
    @TableField(exist=false)
    private List<String> roleNameList ;
    
    /**
     * 角色
     */
    @TableField(exist=false)
    private SysRoleDTO role;
    
    
    /**
     * 角色集合 用于存储用户对应的多个角色
     */
    @TableField(exist=false)
    private List<SysRoleDTO> roleList;
    

    
	public List<String> getRoleNameList() {
		return roleNameList;
	}

	public void setRoleNameList(List<String> roleNameList) {
		this.roleNameList = roleNameList;
	}

	public SysRoleDTO getRole() {
		return role;
	}

	public void setRole(SysRoleDTO role) {
		this.role = role;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(String userInfo) {
        this.userInfo = userInfo;
    }
    
    
    public String getHeadImg() {
		return headImg;
	}

	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}

	public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    

    public List<SysRoleDTO> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<SysRoleDTO> roleList) {
		this.roleList = roleList;
	}

	@Override
    public String toString() {
        return "SysUserDTO{" +
        "id=" + id +
        ", userName=" + userName +
        ", userPassword=" + userPassword +
        ", userEmail=" + userEmail +
        ", userInfo=" + userInfo +
        ", headImg=" + headImg +
        ", createTime=" + createTime +
        "}";
    }
}
