package com.luculent.efficient.sys.user.service;

import com.luculent.efficient.sys.user.dto.SysUserDTO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author zhangyang
 * @since 2019-04-30
 */
public interface SysUserService extends IService<SysUserDTO> {

	/**
	 *  根据用户id查询用户信息带角色信息   
	 *  一对一 resultMap
	 *  association嵌套查询
	 *@param id
	 *@return SysUserDTO
	 */
	SysUserDTO selectUserAndRoleByIdSelect(Long id);
	
}
