package com.luculent.efficient.sys.user.dao;

import com.luculent.efficient.sys.user.dto.SysUserDTO;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author zhangyang
 * @since 2019-04-30
 */
public interface SysUserDAO extends BaseMapper<SysUserDTO> {
	
	/**
	 *  根据用户id查询用户信息带角色名称
	 *  一对一
	 *@param id
	 *@return SysUserDTO
	 */
	SysUserDTO selectUserWithRoleNameById(Long id);

	/**
	 *  根据用户id查询用户信息带角色信息   
	 *  一对一
	 *@param id
	 *@return SysUserDTO
	 */
	SysUserDTO selectUserAndRoleById(Long id);
	
	
	/**
	 *  根据用户id查询用户信息带角色信息   
	 *  一对一 resultMap
	 *@param id
	 *@return SysUserDTO
	 */
	SysUserDTO selectUserAndRoleById2(Long id);
	
	/**
	 *  根据用户id查询用户信息带角色信息   
	 *  一对一 resultMap
	 *  association
	 *@param id
	 *@return SysUserDTO
	 */
	SysUserDTO selectUserAndRoleById3(Long id);
	
	
	/**
	 *  根据用户id查询用户信息带角色信息   
	 *  一对一 resultMap
	 *  association嵌套查询
	 *@param id
	 *@return SysUserDTO
	 */
	SysUserDTO selectUserAndRoleByIdSelect(Long id);
	
	/**
	 * 参数为静态常量
	 *@return SysUserDTO
	 */
	SysUserDTO selectByUserConstant();
	
	
	/**
	 * 参数为静态方法
	 *@return SysUserDTO
	 */
	SysUserDTO selectByUserConstantMethod();
	
	/**
	 * 查询所有用户及其对应的角色 
	 *@return SysUserDTO
	 */
	List<SysUserDTO> selectAllUserAndRoles();
	
	
	/**
	 * 查询所有用户及其对应的角色及权限 
	 *@return SysUserDTO
	 */
	List<SysUserDTO> selectAllUserAndRoles1();
	
	
	/**
	 * 查询所有用户包含其角色名称
	 *@return SysUserDTO
	 */
	List<SysUserDTO> selectUserWithRoleName();
	
	/**
	 * 嵌套查询所有用户及其对应的角色及权限 
	 *@return SysUserDTO
	 */
	List<SysUserDTO> selectAllUserAndRolesSelect(Long userId);
	
	
	
}
