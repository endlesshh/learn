package com.luculent.efficient.sys.user.web;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.extension.api.R;
import com.luculent.efficient.sys.user.dao.SysUserDAO;
import com.luculent.efficient.sys.user.dto.SysUserDTO;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author zhangyang
 * @since 2019-04-30
 */
@RestController
@RequestMapping("/user")
public class SysUserController {
	
	@Autowired
	private SysUserDAO sysUserDAO;

	@GetMapping
	R<SysUserDTO> getuserInfo(){
		SysUserDTO userDTO = sysUserDAO.selectUserAndRoleById(1001l);
		return R.ok(userDTO);
	}
}

