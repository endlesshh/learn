package com.luculent.efficient.sys.user.service.impl;

import com.luculent.efficient.sys.user.dto.SysUserDTO;
import com.luculent.efficient.sys.user.dao.SysUserDAO;
import com.luculent.efficient.sys.user.service.SysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author zhangyang
 * @since 2019-04-30
 */
@Service
@Transactional
public class SysUserServiceImpl extends ServiceImpl<SysUserDAO, SysUserDTO> implements SysUserService {

	@Override
	public SysUserDTO selectUserAndRoleByIdSelect(Long id) {
		SysUserDTO user = this.baseMapper.selectUserAndRoleByIdSelect(id);
		Assert.notNull(user,"user不能为空");
		System.out.println("调用 user.getRole");
		Assert.notNull(user.getRole(),"role为空");
		return null;
	}

}
