package com.luculent.efficient.sys.user;

import java.time.LocalDate;
/**
 * 
 *user常量
 *@author: zhangyang
 *@since: 2019年5月22日下午3:07:33
 */
public final class UserConstant {
	
	private UserConstant() {
		throw new AssertionError();
	}

	public static final long ASSIGN_USER_ID = 1l;
	
	public static final String getCurrentYear() {
		return String.valueOf(LocalDate.now().getYear());
	}
	
}
