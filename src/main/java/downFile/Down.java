package downFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import org.springframework.core.io.ClassPathResource;
/**
 * springboot文件下载示例
 * @author Administrator
 *
 */
public class Down {
	public static void main(String[] args) throws IOException {
		ClassPathResource resource = new ClassPathResource("file/subject.xlsx");
    	Path temp = Files.createTempFile("subject.xlsx", "tmp");
    	Files.copy(resource.getInputStream(), temp, StandardCopyOption.REPLACE_EXISTING);
//        com.ifast.common.utils.FileUtil.fileDownload(response,temp.toFile(),"考题导入模板.xlsx");
	}
}
