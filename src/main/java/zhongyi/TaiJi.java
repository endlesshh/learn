package zhongyi;

import cn.hutool.log.StaticLog;

/**
 * 阴阳未判，一气混茫
 * 阴升阳降
 * 左升右降
 * @author ShiQiang
 */
public interface TaiJi {
	String name = "太极"; 
	public static int yy = 5;
	public default void execute(){
		StaticLog.info("气的其他象:甲骨文->ミ,繁 -> 氣");
	}
}
