package zhongyi;

public enum Names {
	
	Qi("气","卫"),
	Xue("血","营"),
	Jing("精","魄"),
	Shen("神","魂"); 
	
	private String name;
	private String nextName;
	private Names(String name,String nextName){
		this.name = name;
		this.nextName = nextName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNextName() {
		return nextName;
	}
	public void setNextName(String nextName) {
		this.nextName = nextName;
	} 
}
