package zhongyi;
/**
 * 河图五行术数
 * @author ShiQiang 
 */
public enum HeTu{
	Shui("水",1,6),
	//2  2
	Mu("木",3,8),
	//4   -6
	Huo("火",7,2),
	//-2  8
	Tu("土",5,10),
	//4    -6
	Jin("金",9,4);
	// -8  2
	private String name;
	private int yang;
	private int yin;
	
	private HeTu(String name,int yang,int yin){ 
		this.name = name;
		this.yang = yang;
		this.yin = yin;
	}
	public int getYin() {
		return yin;
	}
	public void setYin(int yin) {
		this.yin = yin;
	}
	public int getYang() {
		return yang;
	}
	public void setYang(int yang) {
		this.yang = yang;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
