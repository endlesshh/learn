package zhongyi;
/**
 * 洛书 八卦术数
 * @author ShiQiang
 *
 */
public enum LuoShu{
	JuYin(2),
	ShaoYang(8),
	ShaoYin(3),
	TaiYang(7),
	TaiYin(6),
	YangMing(4),
	TaiJi(5);
	private int qi;
	private LuoShu(int qi){
		this.qi = qi;
	}
	public int getQi() {
		return qi;
	}
	public void setQi(int qi) {
		this.qi = qi;
	}
}
