package zhongyi;

public enum TianGan{
	
	jia("甲木"),
	yi("乙木"),
	bing("丙火"),
	ding("丁火"),
	wu("戊土"),
	ji("己土"),
	gen("庚金"),
	xin("辛金"),
	ren("壬水"),
	gui("癸水");
	
	private String name;
	
	private TianGan(String name){
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
