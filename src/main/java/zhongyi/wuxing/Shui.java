package zhongyi.wuxing;

import cn.hutool.log.StaticLog;

/**
 * 一六共宗，为水居北 ->玄武
 * @author ShiQiang
 */
public class Shui extends WuXing{
	public int yang = 1;
	public int yin = 6;
	
	public Shui(){
		StaticLog.info("水气中含有 阴:{} ↔ 阳:{}",yin,yang);
	}
	
	public Mu shengMu(){
		StaticLog.info("水 生 木");
		return new Mu();
	}
	
	public Huo keHuo(){
		StaticLog.info("水 克 火");
		return new Huo();
	}
}
