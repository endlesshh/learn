package zhongyi.wuxing;

import cn.hutool.log.StaticLog;

/**
 * 五十同途，为土居中 ->时空奇点
 * @author ShiQiang
 */
public class Tu extends WuXing{
	public int yang = 5;
	public int yin = 10;
	
	public Tu(){
		StaticLog.info("土气中含有 阴:{} ↔ 阳:{}",yin,yang);
	}
	
	public Jin shengJin(){
		StaticLog.info("土 生 金");
		return new Jin();
	} 
	
	public Shui keShui(){
		StaticLog.info("土 克 水");
		return new Shui();
	}
}
