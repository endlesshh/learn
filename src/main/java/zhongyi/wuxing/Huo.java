package zhongyi.wuxing;

import cn.hutool.log.StaticLog;

/**
 * 二七同道，为火居南 ->朱雀
 * @author ShiQiang
 *
 */
public class Huo extends WuXing{
	public int yang = 7;
	public int yin = 2;
	
	public Huo(){ 
		StaticLog.info("火气中含有 阴:{} ↔ 阳:{}",yin,yang);
	}
	
	public Tu shengTu(){
		StaticLog.info("火 生 土");
		return new Tu();
	}
	
	public Jin keJin(){
		StaticLog.info("火 克 金");
		return new Jin();
	}
}
