package zhongyi.wuxing;

import cn.hutool.log.StaticLog;

/**
 * 四九为友，为金居西 ->白虎
 * @author ShiQiang
 */
public class Jin extends WuXing{
	public int yang = 9;
	public int yin = 4;
	
	public Jin(){
		StaticLog.info("金气中含有 阴:{} ↔ 阳:{}",yin,yang);
	}
	
	public Shui shengShui(){
		StaticLog.info("金 生 水");
		return new Shui();
	}
	
	public Mu keMu(){
		StaticLog.info("金 克 木");
		return new Mu();
	}
}
