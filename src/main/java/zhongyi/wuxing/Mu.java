package zhongyi.wuxing;

import cn.hutool.log.StaticLog;

/**
 * 三八为朋，为木居东 ->青龙
 * @author ShiQiang
 */
public class Mu extends WuXing{
	public int yang = 3;
	public int yin = 8;
	
	public Mu(){
		StaticLog.info("木气中含有 阴:{} ↔ 阳:{}",yin,yang);
	}
	public Huo shengHuo(){
		StaticLog.info("木 生 火");
		return new Huo();
	}
	
	public Tu keTu(){
		StaticLog.info("木 克 土");
		return new Tu();
	}
}
