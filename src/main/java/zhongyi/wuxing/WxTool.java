package zhongyi.wuxing;

public class WxTool{
	
	//水生木
	public static Mu shuiToMu(Shui shui){
		return new Mu();
	}
	//木生火
	public static Huo muToJHuo(Mu mu){
		return new Huo();
	}
	//火生土
	public static Tu huoToTu(Huo jHuo){
		return new Tu();
	}
	//土生金
	public static Jin tuToJin(Tu tu){
		return new Jin();
	}
	//金生水
	public static Shui jinToShui(Jin tu){
		return new Shui();
	}
	
	//木克土
	public static Shui muKeTu(Jin tu){
		return new Shui();
	}
}
