package zhongyi.other.eat;
/**
 * 能食用的
 * @author ShiQiang
 */
public interface Edible{
	public String getName();
}
