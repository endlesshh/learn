package zhongyi.other.eat.food;

import zhongyi.other.eat.Edible;

public class Grain implements Edible{

	public Grain(String name){
		this.name = name;
	} 
	private String name;
	
	@Override
	public String getName() { 
		return this.name;
	}
	
}
