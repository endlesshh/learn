package zhongyi.other.eat.food;

import zhongyi.other.eat.Edible;

public class Water implements Edible{
	
	private String name;
	
	public Water(String name){
		this.name = name;
	}
	
	@Override
	public String getName() { 
		return this.name;
	}

}
