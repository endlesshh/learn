package zhongyi.ren.zang;

import com.google.common.collect.Range;

import cn.hutool.log.StaticLog;
import zhongyi.TianGan;
import zhongyi.Yin;
import zhongyi.ren.five.WuSe;
import zhongyi.ren.jingshengqixue.Jing;
import zhongyi.ren.jingshengqixue.Qi;
import zhongyi.ren.jingshengqixue.Shen;
import zhongyi.ren.jingshengqixue.Xue;
import zhongyi.ren.shengti.JinZhua;
import zhongyi.ren.shengti.Yan;
import zhongyi.wuxing.Mu;
/**
 * 肝 肝经
 * @author ShiQiang
 *
 */
public class Gan_Zang extends Mu implements Zang, Yin{
	
	public static TianGan tGan = TianGan.yi;  
	
	public static int upDown = 1;
	/**
	 * 肝主筋，其荣爪
	 */
	private JinZhua jinZhua;
	/**
	 * 肝窍于目
	 */
	private Yan yan; 
	/**
	 * 悉受之于肾
	 */
	private Jing<?> jing; 
	/**
	 * 悉受之于心
	 */
	private Shen<?> shen;
	/**
	 * 悉受之于肺  
	 */
	private Qi<?> qi; 
	/**
	 * 肝藏血 
	 */
	private Xue<?> xue;
	public void store(Xue<?> xue){
		StaticLog.info("血藏于肝");
		this.xue = xue;
		xuanBu();
		createShen();
	}
	
	/**
	 * 肝血温升，则化阳神
	 * @return
	 */
	public void createShen(){
		StaticLog.info("肝血温升，则化阳神"); 
		Xin_Zang.getInstance().store(Shen.getInstance());
	}
	
	/**
	 * 五藏皆有血，悉受之于肝
	 */
	public void xuanBu(){
		StaticLog.info("五藏皆有血，悉受之于肝");
		Fei_Zang.getInstance().setXue(this.xue);
		Xin_Zang.getInstance().setXue(this.xue);
		Pi_Zang.getInstance().setXue(this.xue);
		Shen_Zang.getInstance().setXue(this.xue);
	}
	 
	/**
	 * 肝主五色
	 * @return
	 */
	public String color(Object ob){
		String yanSe = WuSe.get(ob);
		StaticLog.info("脏的颜色:{}",yanSe);
		return yanSe;
	} 
	
	@Override
	public String getColor(Gan_Zang gan) { 
		return gan.color(this);
	}
	
	@Override
	public String getVoice(Fei_Zang fei) { 
		return fei.voice(this);
	} 
	
	@Override
	public String getTaste(Pi_Zang pi) { 
		return pi.taste(this);
	}
	
	@Override
	public String getSmell(Xin_Zang xin) { 
		return xin.smell(this);
	}
	
	@Override
	public String getFluid(Shen_Zang shen) { 
		return shen.fluid(this);
	}
	
	@Override
	public String getFeeling(Shen<?> shen) { 
		return shen.feeling(this);
	}  
	/**
	 * 营血注流
	 */
	private Range<Integer> zi = Range.closedOpen(1, 3); 
	public Fei_Zang f_1_3_t_Fei(){
		StaticLog.info("气血在肝经的时间 从:{} -> {}",zi.lowerEndpoint(),zi.upperEndpoint());
		return Fei_Zang.getInstance();
	}
	
	public Pi_Zang toPi(){
		StaticLog.info("肝注于脾");
		return Pi_Zang.getInstance();
	}
	
	public Xue<?> getXue() {
		return xue;
	}
	public void setXue(Xue<?> xue) {
		this.xue = xue;
	}
	
	public JinZhua getJinZhua() {
		return jinZhua;
	}
	public void setJinZhua(JinZhua jinZhua) {
		this.jinZhua = jinZhua;
	}
	public Yan getYan() {
		return yan;
	}
	public void setYan(Yan yan) {
		this.yan = yan;
	}
	/**
	 * 单例模式保证唯一
	 */
	private Gan_Zang() {} 
    private static class SingletonInstance {
        private static final Gan_Zang INSTANCE = new Gan_Zang();
    } 
    public static Gan_Zang getInstance() {
        return SingletonInstance.INSTANCE;
    }

	public Jing<?> getJing() {
		return jing;
	}

	public void setJing(Jing<?> jing) {
		this.jing = jing;
	}

	public Shen<?> getShen() {
		return shen;
	}

	public void setShen(Shen<?> shen) {
		this.shen = shen;
	}

	public Qi<?> getQi() {
		return qi;
	}

	public void setQi(Qi<?> qi) {
		this.qi = qi;
	}
    
}
