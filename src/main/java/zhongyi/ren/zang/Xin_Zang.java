package zhongyi.ren.zang;

import com.google.common.collect.Range;

import cn.hutool.log.StaticLog;
import zhongyi.TianGan;
import zhongyi.Yin;
import zhongyi.ren.five.WuXiu;
import zhongyi.ren.fu.XiaoChang_Fu;
import zhongyi.ren.jingshengqixue.Jing;
import zhongyi.ren.jingshengqixue.Qi;
import zhongyi.ren.jingshengqixue.Shen;
import zhongyi.ren.jingshengqixue.Xue;
import zhongyi.ren.shengti.MaiShe;
import zhongyi.ren.shengti.She;
import zhongyi.wuxing.Huo;
/**
 * 心 心经
 * @author ShiQiang
 *
 */
public class Xin_Zang extends Huo implements Zang, Yin{
	
	public static TianGan tGan = TianGan.ding;  
	
	public static int upDown = -1;
	/**
	 * 心主脉，其荣色
	 */
	private MaiShe maiShe;
	/**
	 * 心窍于舌
	 */
	private She she;
	
	/**
	 * 悉受之于肾
	 */
	private Jing<?> jing; 
	 
	/**
	 * 悉受之于肺  
	 */
	private Qi<?> qi; 
	/**
	 * 悉受之于肝
	 */
	private Xue<?> xue;
	/**
	 * 心藏神
	 */
	private Shen<?> shen;
	/**
	 * 神发于心
	 * @param sheng
	 */
	public void store(Shen<?> shen){
		StaticLog.info("神发于心");
		this.shen = shen;
		xuanBu();
	}
	
	/**
	 * 五藏皆有神，悉受之于心
	 */
	public void xuanBu(){
		StaticLog.info("五藏皆有神，悉受之于心");
		Fei_Zang.getInstance().setShen(this.shen);
		Gan_Zang.getInstance().setShen(this.shen);
		Pi_Zang.getInstance().setShen(this.shen);
		Shen_Zang.getInstance().setShen(this.shen);
	}
	
	/**
	 * 心主五臭
	 * @return
	 */
	public String smell(Object ob){
		String yanSe = WuXiu.get(ob);
		StaticLog.info("脏的臭:{}",yanSe);
		return yanSe;
	}
	
	@Override
	public String getColor(Gan_Zang gan) { 
		return gan.color(this);
	}
	
	@Override
	public String getVoice(Fei_Zang fei) { 
		return fei.voice(this);
	} 
	
	@Override
	public String getTaste(Pi_Zang pi) { 
		return pi.taste(this);
	}
	
	@Override
	public String getSmell(Xin_Zang xin) { 
		return xin.smell(this);
	}
	
	@Override
	public String getFluid(Shen_Zang shen) { 
		return shen.fluid(this);
	}
	
	@Override
	public String getFeeling(Shen<?> shen) { 
		return shen.feeling(this);
	} 
	
	
	private Range<Integer> zi = Range.closedOpen(11, 13); 
	public XiaoChang_Fu f_11_13_t_XiaoChang(){
		StaticLog.info("气血在心经的时间 从:{} -> {}",zi.lowerEndpoint(),zi.upperEndpoint());
		return XiaoChang_Fu.getInstance();
	} 
	
	public Fei_Zang toFei(){
		StaticLog.info("心注于肺");
		return Fei_Zang.getInstance();
	}

	public Shen<?> getShen() {
		return shen;
	}

	public void setShen(Shen<?> shen) {
		this.shen = shen;
	}

	public MaiShe getMaiShe() {
		return maiShe;
	}

	public void setMaiShe(MaiShe maiShe) {
		this.maiShe = maiShe;
	}

	public She getShe() {
		return she;
	}

	public void setShe(She she) {
		this.she = she;
	}
	
	/**
	 * 单例模式保证唯一
	 */
	private Xin_Zang() {} 
    private static class SingletonInstance {
        private static final Xin_Zang INSTANCE = new Xin_Zang();
    } 
    public static Xin_Zang getInstance() {
        return SingletonInstance.INSTANCE;
    }

	public Jing<?> getJing() {
		return jing;
	}

	public void setJing(Jing<?> jing) {
		this.jing = jing;
	}

	public Qi<?> getQi() {
		return qi;
	}

	public void setQi(Qi<?> qi) {
		this.qi = qi;
	}

	public Xue<?> getXue() {
		return xue;
	}

	public void setXue(Xue<?> xue) {
		this.xue = xue;
	}
}
