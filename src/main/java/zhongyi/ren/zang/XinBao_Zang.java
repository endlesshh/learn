package zhongyi.ren.zang;

import com.google.common.collect.Range;

import cn.hutool.log.StaticLog;
import zhongyi.Yin;
import zhongyi.ZangFu;
import zhongyi.ren.fu.SanJiao_Fu;
import zhongyi.wuxing.Huo;
/**
 * 心包 心包经
 * @author ShiQiang
 *
 */
public class XinBao_Zang extends Huo implements ZangFu, Yin{
	
	public static int upDown = -1;
	
	private Range<Integer> zi = Range.closedOpen(19, 21);
	
	public SanJiao_Fu f_19_21_t_SanJiao(){
		StaticLog.info("气血在心包经的时间 从:{} -> {}",zi.lowerEndpoint(),zi.upperEndpoint());
		return SanJiao_Fu.getInstance();
	}
	/**
	 * 单例模式保证唯一
	 */
	private XinBao_Zang() {} 
    private static class SingletonInstance {
        private static final XinBao_Zang INSTANCE = new XinBao_Zang();
    } 
    public static XinBao_Zang getInstance() {
        return SingletonInstance.INSTANCE;
    }
}
