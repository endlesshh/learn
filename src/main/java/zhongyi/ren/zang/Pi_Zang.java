package zhongyi.ren.zang;

import java.util.List;

import com.google.common.collect.Range;

import cn.hutool.log.StaticLog;
import zhongyi.TianGan;
import zhongyi.Yin;
import zhongyi.other.eat.Edible;
import zhongyi.ren.five.WuWei;
import zhongyi.ren.fu.Wei_Fu;
import zhongyi.ren.fu.XiaoChang_Fu;
import zhongyi.ren.jingshengqixue.Jing;
import zhongyi.ren.jingshengqixue.Qi;
import zhongyi.ren.jingshengqixue.Shen;
import zhongyi.ren.jingshengqixue.Xue;
import zhongyi.ren.shengti.Kou;
import zhongyi.ren.shengti.RouChun;
import zhongyi.wuxing.Tu;
/**
 * 脾 脾经
 * @author ShiQiang
 *
 */
public class Pi_Zang extends Tu implements Zang, Yin{
	
	public static TianGan tGan = TianGan.ji;  
	
	public static int upDown = 1;
	/**
	 * 脾以纯阴而含阳气 - 清阳上升，是以温暖而善消磨
	 *  自己理解 -- 脾恒温内温 -> 脏藏，温度高，
	 */ 
	public int yang = super.yang;
	/**
	 * 脾主肉，其荣唇
	 */
	private RouChun rouChun;
	/**
	 * 脾窍于口
	 */
	private Kou kou;
	
	/**
	 * 互为表里
	 */
	private Wei_Fu wei;
	
	/**
	 * 悉受之于肾
	 */
	private Jing<?> jing; 
	/**
	 * 悉受之于心
	 */
	private Shen<?> shen;
	/**
	 * 悉受之于肺  
	 */
	private Qi<?> qi; 
	/**
	 * 悉受之于肝
	 */
	private Xue<?> xue; 
	
	/**
	 * 脾阳磨化
	 */
	public XiaoChang_Fu piMill(){ 
		StringBuilder build = new StringBuilder(); 
		List<Edible> foods = wei.getEats();
		foods.stream().forEach(food -> {
			build.append(food.getName()).append("_");
		});
		StaticLog.info("脾阳磨化 - 胃中的食物： {}",build.toString());
		return XiaoChang_Fu.getInstance();
	} 

	/**
	 * 脾为生血之本
	 * @return
	 */
	public void createXue(){
		StaticLog.info("脾为生血之本");
		StaticLog.info("脾阳磨化 ：{}",yang);
		Gan_Zang.getInstance().store(Xue.getInstance()); 
	} 
	
	/**
	 * 脾主五味
	 * @return
	 */
	public String taste(Object ob){
		String yanSe = WuWei.get(ob);
		StaticLog.info("脏的味:{}",yanSe);
		return yanSe;
	} 
	
	@Override
	public String getColor(Gan_Zang gan) { 
		return gan.color(this);
	}
	
	@Override
	public String getVoice(Fei_Zang fei) { 
		return fei.voice(this);
	} 
	
	@Override
	public String getTaste(Pi_Zang pi) { 
		return pi.taste(this);
	}
	
	@Override
	public String getSmell(Xin_Zang xin) { 
		return xin.smell(this);
	}
	
	@Override
	public String getFluid(Shen_Zang shen) { 
		return shen.fluid(this);
	}
	
	@Override
	public String getFeeling(Shen<?> shen) { 
		return shen.feeling(this);
	} 
	
	private Range<Integer> zi = Range.closedOpen(9, 11); 
	public Xin_Zang f_9_11_t_Xin(){
		StaticLog.info("气血在脾经的时间 从:{} -> {}",zi.lowerEndpoint(),zi.upperEndpoint());
		return Xin_Zang.getInstance();
	}
	
	public Shen_Zang toShen(){
		StaticLog.info("脾复注于肾");
		return Shen_Zang.getInstance();
	}
	
	public RouChun getRouChun() {
		return rouChun;
	}
	
	public void setRouChun(RouChun rouChun) {
		this.rouChun = rouChun;
	}

	public Kou getKou() {
		return kou;
	}

	public void setKou(Kou kou) {
		this.kou = kou;
	}
	/**
	 * 单例模式保证唯一
	 */ 
	private Pi_Zang() {} 
    private static class SingletonInstance {
        private static final Pi_Zang INSTANCE = new Pi_Zang();
    } 
    public static Pi_Zang getInstance() { 
        return SingletonInstance.INSTANCE;
    }
    
    public Wei_Fu getWei() {
		return wei;
	} 
	public void setWei(Wei_Fu wei) {
		this.wei = wei;
	}

	public Jing<?> getJing() {
		return jing;
	}

	public void setJing(Jing<?> jing) {
		this.jing = jing;
	}

	public Shen<?> getShen() {
		return shen;
	}

	public void setShen(Shen<?> shen) {
		this.shen = shen;
	}

	public Qi<?> getQi() {
		return qi;
	}

	public void setQi(Qi<?> qi) {
		this.qi = qi;
	}

	public Xue<?> getXue() {
		return xue;
	}

	public void setXue(Xue<?> xue) {
		this.xue = xue;
	}
	
}
