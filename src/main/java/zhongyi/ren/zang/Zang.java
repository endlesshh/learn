package zhongyi.ren.zang;

import zhongyi.ZangFu;
import zhongyi.ren.jingshengqixue.Shen;

public interface Zang extends ZangFu{
	/**
	 * 色
	 */
	public String getColor(Gan_Zang gan);
	/**
	 * 声
	 */
	public String getVoice(Fei_Zang fei);
	/**
	 * 味
	 */
	public String getTaste(Pi_Zang pi);
	/**
	 * 臭
	 */
	public String getSmell(Xin_Zang xin);
	/**
	 * 液
	 */
	public String getFluid(Shen_Zang shen);
	/**
	 * 情志
	 * @return
	 */
	public String getFeeling(Shen<?> shen);
	
}
