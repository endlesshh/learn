package zhongyi.ren.zang;

import com.google.common.collect.Range;

import cn.hutool.log.StaticLog;
import zhongyi.TianGan;
import zhongyi.Yin;
import zhongyi.ren.five.WuYe;
import zhongyi.ren.fu.PangGuang_Fu;
import zhongyi.ren.jingshengqixue.Jing;
import zhongyi.ren.jingshengqixue.Qi;
import zhongyi.ren.jingshengqixue.Shen;
import zhongyi.ren.jingshengqixue.Xue;
import zhongyi.ren.shengti.Er;
import zhongyi.ren.shengti.GuFa;
import zhongyi.wuxing.Shui;
/**
 * 肾 肾经
 * @author ShiQiang
 *
 */
public class Shen_Zang extends Shui implements Zang, Yin{
	
	public static TianGan tGan = TianGan.gui;  
	
	public static int upDown = 1;
	/**
	 * 肾主骨，其荣发
	 */
	private GuFa guFa;
	/**
	 * 肾窍于耳
	 */
	private Er er;
	 
	/**
	 * 悉受之于心
	 */
	private Shen<?> shen;
	/**
	 * 悉受之于肺  
	 */
	private Qi<?> qi; 
	/**
	 * 悉受之于肝
	 */
	private Xue<?> xue;
	/**
	 * 精藏于肾 
	 */
	private Jing<?> jing;
	
	public void store(Jing<?> jing){
		StaticLog.info("精藏于肾");
		this.jing = jing;
		xuanBu();
	} 
	/**
	 * 五藏皆有精，悉受之于肾
	 */
	public void xuanBu(){
		StaticLog.info("五藏皆有精，悉受之于肾");
		Fei_Zang.getInstance().setJing(this.jing);
		Gan_Zang.getInstance().setJing(this.jing);
		Pi_Zang.getInstance().setJing(this.jing);
		Xin_Zang.getInstance().setJing(this.jing);
	}
	
	/**
	 * 肾主五液
	 * @return
	 */
	public String fluid(Object ob){
		String yanSe = WuYe.get(ob);
		StaticLog.info("脏的液:{}",yanSe);
		return yanSe;
	}
	
	@Override
	public String getColor(Gan_Zang gan) { 
		return gan.color(this);
	}
	
	@Override
	public String getVoice(Fei_Zang fei) { 
		return fei.voice(this);
	} 
	
	@Override
	public String getTaste(Pi_Zang pi) { 
		return pi.taste(this);
	}
	
	@Override
	public String getSmell(Xin_Zang xin) { 
		return xin.smell(this);
	}
	
	@Override
	public String getFluid(Shen_Zang shen) { 
		return shen.fluid(this);
	}
	
	@Override
	public String getFeeling(Shen<?> shen) { 
		return shen.feeling(this);
	} 
	
	
	private Range<Integer> zi = Range.closedOpen(17, 19); 
	public XinBao_Zang f_17_19_t_XinBao(){
		StaticLog.info("气血在肾经的时间 从:{} -> {}",zi.lowerEndpoint(),zi.upperEndpoint());
		return XinBao_Zang.getInstance();
	}
	
	public PangGuang_Fu toZuTaiYangPangGuang(){
		StaticLog.info("下入足少阴经。阴蹻者，足少阴之别，属于目内眦。自阴蹻而复合于目，交于足太阳之睛明，是谓一周。如此者二十五周，日入阳尽，而阴受气矣，于是内入于阴藏");
		return PangGuang_Fu.getInstance();
	}
	
	public Object toXin(int i){
		StaticLog.info("循环完成第"+i+"周");
		if(i<=50){
			StaticLog.info("常从足少阴之经而注于肾，肾注于心");
			return Xin_Zang.getInstance();
		}else{
			StaticLog.info(i+"平旦阴尽而阳受气矣，于是外出于阳经。其出于阳也，常从肾至足少阴之经，而复合于目");
			return PangGuang_Fu.getInstance();
		}
		
	}
	
	
	public Jing<?> getJing() {
		return jing;
	}
	public void setJing(Jing<?> jing) {
		this.jing = jing;
	}

	public GuFa getGuFa() {
		return guFa;
	}

	public void setGuFa(GuFa guFa) {
		this.guFa = guFa;
	}

	public Er getEr() {
		return er;
	}

	public void setEr(Er er) {
		this.er = er;
	} 
	/**
	 * 单例模式保证唯一
	 */
	private Shen_Zang() {} 
    private static class SingletonInstance {
        private static final Shen_Zang INSTANCE = new Shen_Zang();
    } 
    public static Shen_Zang getInstance() {
        return SingletonInstance.INSTANCE;
    }

	public Shen<?> getShen() {
		return shen;
	}

	public void setShen(Shen<?> shen) {
		this.shen = shen;
	}

	public Qi<?> getQi() {
		return qi;
	}

	public void setQi(Qi<?> qi) {
		this.qi = qi;
	}

	public Xue<?> getXue() {
		return xue;
	}

	public void setXue(Xue<?> xue) {
		this.xue = xue;
	}
}
