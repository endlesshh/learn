package zhongyi.ren.zang;

import com.google.common.collect.Range;

import cn.hutool.log.StaticLog;
import zhongyi.TianGan;
import zhongyi.Yin;
import zhongyi.ren.five.WuSheng;
import zhongyi.ren.fu.DaChang_Fu;
import zhongyi.ren.jingshengqixue.Jing;
import zhongyi.ren.jingshengqixue.Qi;
import zhongyi.ren.jingshengqixue.Shen;
import zhongyi.ren.jingshengqixue.Xue;
import zhongyi.ren.shengti.Bi;
import zhongyi.ren.shengti.PiMao;
import zhongyi.wuxing.Jin;
/**
 * 肺 肺经
 * @author ShiQiang
 *
 */
public class Fei_Zang extends Jin implements Zang, Yin{
	
	public static TianGan tGan = TianGan.xin;  
	
	public static int upDown = -1;
	/**
	 * 肺主皮，其荣毛
	 */
	private PiMao piMao;
	/**
	 * 肺窍于鼻
	 */
	private Bi bi;
	
	/**
	 * 悉受之于肾
	 */
	private Jing<?> jing;
	/**
	 * 悉受之于肝
	 */
	private Xue<?> xue;
	/**
	 * 悉受之于心
	 */
	private Shen<?> shen;
	/**
	 * 肺藏气  
	 */
	private Qi<?> qi; 
	
	public void store(Qi<?> qi){
		StaticLog.info("气统于肺");
		this.qi = qi;
		xuanBu();
		createJing();
	}
	/**
	 * 肺气清降，则产阴精
	 * @return
	 */
	public void createJing(){
		StaticLog.info("肺气清降，则产阴精"); 
		StaticLog.info("精者入于脏腑而为津液"); 
		Shen_Zang.getInstance().store(Jing.getInstance());
	}
	/**
	 * 五藏皆有气，悉受之于肺
	 */
	public void xuanBu(){
		StaticLog.info("五藏皆有气，悉受之于肺");
		Gan_Zang.getInstance().setQi(this.qi);
		Xin_Zang.getInstance().setQi(this.qi);
		Pi_Zang.getInstance().setQi(this.qi);
		Shen_Zang.getInstance().setQi(this.qi);
	}
	/**
	 * 肺主五声
	 * @param ob
	 * @return
	 */
	public String voice(Object ob){
		String shengYin = WuSheng.get(ob);
		StaticLog.info("脏的声:{}",shengYin);
		return shengYin;
	}
	
	@Override
	public String getColor(Gan_Zang gan) { 
		return gan.color(this);
	}
	
	@Override
	public String getVoice(Fei_Zang fei) { 
		return fei.voice(this);
	} 
	
	@Override
	public String getTaste(Pi_Zang pi) { 
		return pi.taste(this);
	}
	
	@Override
	public String getSmell(Xin_Zang xin) { 
		return xin.smell(this);
	}
	
	@Override
	public String getFluid(Shen_Zang shen) { 
		return shen.fluid(this);
	}
	
	@Override
	public String getFeeling(Shen<?> shen) { 
		return shen.feeling(this);
	} 
	
	private Range<Integer> zi = Range.closedOpen(3, 5); 
	public DaChang_Fu f_3_5_t_DaChang(){
		StaticLog.info("气血在肺经的时间 从:{} -> {}",zi.lowerEndpoint(),zi.upperEndpoint());
		return DaChang_Fu.getInstance();
	}
	
	public Gan_Zang toGan(){
		StaticLog.info("肺注于肝");
		return Gan_Zang.getInstance();
	}
	
	public Qi<?> getQi() {
		return qi;
	}
	public void setQi(Qi<?> qi) {
		this.qi = qi;
	}
	
	public PiMao getPiMao() {
		return piMao;
	}
	public void setPiMao(PiMao piMao) {
		this.piMao = piMao;
	}
	public Bi getBi() {
		return bi;
	}
	public void setBi(Bi bi) {
		this.bi = bi;
	}
	/**
	 * 单例模式保证唯一
	 */
	private Fei_Zang() {} 
    private static class SingletonInstance {
        private static final Fei_Zang INSTANCE = new Fei_Zang();
    } 
    public static Fei_Zang getInstance() {
        return SingletonInstance.INSTANCE;
    }
	public Jing<?> getJing() {
		return jing;
	}
	public void setJing(Jing<?> jing) {
		this.jing = jing;
	}
	public Xue<?> getXue() {
		return xue;
	}
	public void setXue(Xue<?> xue) {
		this.xue = xue;
	}
	public Shen<?> getShen() {
		return shen;
	}
	public void setShen(Shen<?> shen) {
		this.shen = shen;
	} 
}
