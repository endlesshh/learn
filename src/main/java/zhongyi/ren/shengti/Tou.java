package zhongyi.ren.shengti;
/**
 * 头
 * @author ShiQiang
 *
 */
public class Tou{
	
	private Yan yan;
	private Kou kou;
	private Bi bi;
	private She she;
	private Er er;
	
	public Yan getYan() {
		return yan;
	}
	public void setYan(Yan yan) {
		this.yan = yan;
	}
	public Kou getKou() {
		return kou;
	}
	public void setKou(Kou kou) {
		this.kou = kou;
	}
	public Bi getBi() {
		return bi;
	}
	public void setBi(Bi bi) {
		this.bi = bi;
	}
	public She getShe() {
		return she;
	}
	public void setShe(She she) {
		this.she = she;
	}
	public Er getEr() {
		return er;
	}
	public void setEr(Er er) {
		this.er = er;
	}
}
