package zhongyi.ren;

import com.google.common.collect.Lists;

import zhongyi.other.eat.food.Grain;
import zhongyi.other.eat.food.Water;
import zhongyi.ren.fu.Dan_Fu;
import zhongyi.ren.fu.PangGuang_Fu;
import zhongyi.ren.fu.Wei_Fu;
import zhongyi.ren.zang.Gan_Zang;
import zhongyi.ren.zang.Shen_Zang;
import zhongyi.ren.zang.Xin_Zang;
import zhongyi.wuxing.Shui;

public class Ren {
	/**
	 * 五行生克
	 */
	public static void shengKe(){
		Shui shui = new Shui();
		shui.shengMu().shengHuo().shengTu().shengJin().shengShui();
		shui.keHuo().keJin().keMu().keTu().keShui();
	}
	/**
	 * 子午注流
	 */
	public static void ziWuZhuLiu(){
		Dan_Fu dan = Dan_Fu.getInstance();
		dan.f_23_1_t_Gan()
		   .f_1_3_t_Fei()
		   .f_3_5_t_DaChang()
		   .f_5_7_t_Wei()
		   .f_7_9_t_Pi()
		   .f_9_11_t_Xin()
		   .f_11_13_t_XiaoChang()
		   .f_13_15_t_PangGuang()
		   .f_15_17_t_Sheng()
		   .f_17_19_t_XinBao()
		   .f_19_21_t_SanJiao()
		   .f_21_23_t_Dan();
	}
	/**
	 * 卫气运行
	 */
	public static void weiQiYunXing(){
		Object ob = null; 
		for(int i=1;i<=25;i++){
			ob = PangGuang_Fu.getInstance().toShouTaiYangXiaoChang()
				.toZuShaoYangDan()
				.toShouShaoYangSanJiao()
				.toZuYangMingWei()
				.toShouYangMingDaChang(i);
			if(ob instanceof Shen_Zang){
				 break;
			} 
		}
		for(int i=25;i<=50;i++){
			ob = ((Shen_Zang) ob).toXin(i);
			if(ob instanceof PangGuang_Fu){
				break;
			}
			ob = ((Xin_Zang) ob).toFei().toGan().toPi().toShen();
		}
	}
	/**
	 * 五行注入
	 */
	public static void fiveQiSet(){
		Gan_Zang gan = Gan_Zang.getInstance();
		gan.getColor(gan);
	}
	/**
	 * 精华滋生 - 糟粕传导
	 */
	public static void picked(){
		Wei_Fu wei = Wei_Fu.getInstance(); 
		wei.eat(Lists.newArrayList(new Grain("米"),new Water("汤")))
			.piMill().xiaoChangabsorb(wei.getPi(),wei).daChangdistinguish();
		
	}
	
	public static void main(String[] args) {
		//shengKe(); 		//--五行生克
		//ziWuZhuLiu(); 	//--子午注流
		//fiveQiSet();  	//--五气分注
		//picked(); 		//--精华滋生
		//weiQiYunXing();   //--卫气运行
	}
}
