package zhongyi.ren.fu;

import com.google.common.collect.Range;

import cn.hutool.log.StaticLog;
import zhongyi.TianGan;
import zhongyi.Yang;
import zhongyi.ZangFu;
import zhongyi.other.drop.Faeces;
import zhongyi.other.drop.Pee;
import zhongyi.other.stay.Mixture;
import zhongyi.ren.zang.Shen_Zang;
import zhongyi.wuxing.Jin;
/**
 * 大肠 包含 大肠经
 * @author ShiQiang
 *
 */
public class DaChang_Fu extends Jin implements ZangFu, Yang{
	
	public static TianGan tGan = TianGan.gen;  
	
	public static int upDown = 1;  
	
	private Range<Integer> zi = Range.closedOpen(5, 7);
	public Wei_Fu f_5_7_t_Wei(){
		StaticLog.info("气血在大肠经的时间 从:{} -> {}",zi.lowerEndpoint(),zi.upperEndpoint());
		return Wei_Fu.getInstance();
	}
	
	public Shen_Zang toZuShaoYinShen(){
		StaticLog.info("气血在大肠经的时间 从:{} -> {}",zi.lowerEndpoint(),zi.upperEndpoint());
		return Shen_Zang.getInstance();
	}
	/**
	 * 粪
	 */
	private Faeces faeces;
	private Mixture mixture;
	
	public void daChangdistinguish(){ 
		StaticLog.info("粗者入于膀胱而为溲溺"); 
		PangGuang_Fu.getInstance().save(createPee(this.getMixture()));
		StaticLog.info("溲溺通利，胃无停水，糟粕后传，是以便干"); 
		createFaeces(this.getMixture());
	}
	
	public Faeces createFaeces(Mixture mixture){ 
		return new Faeces("粪便");
	}
	public Pee createPee(Mixture mixture){ 
		return new Pee("尿液");
	}
	
	public Faeces getFaeces() {
		return faeces;
	}
	public void setFaeces(Faeces faeces) {
		this.faeces = faeces;
	}
	
	public Mixture getMixture() {
		return mixture;
	}

	public void setMixture(Mixture mixture) {
		this.mixture = mixture;
	}

	/**
	 * 单例模式保证唯一
	 */
	private DaChang_Fu() {} 
    private static class SingletonInstance {
        private static final DaChang_Fu INSTANCE = new DaChang_Fu();
    } 
    public static DaChang_Fu getInstance() {
        return SingletonInstance.INSTANCE;
    }
}
