package zhongyi.ren.fu;

import com.google.common.collect.Range;

import cn.hutool.log.StaticLog;
import zhongyi.TianGan;
import zhongyi.Yang;
import zhongyi.ZangFu;
import zhongyi.other.stay.Mixture;
import zhongyi.ren.zang.Pi_Zang;
import zhongyi.wuxing.Huo;
/**
 * 小肠 小肠经
 * @author ShiQiang
 *
 */
public class XiaoChang_Fu extends Huo implements ZangFu, Yang{
	
	public static TianGan tGan = TianGan.bing;  
	
	public static int upDown = 1;
	
	private Range<Integer> zi = Range.closedOpen(13, 15); 
	public PangGuang_Fu f_13_15_t_PangGuang(){
		StaticLog.info("气血在小肠经的时间 从:{} -> {}",zi.lowerEndpoint(),zi.upperEndpoint());
		return PangGuang_Fu.getInstance();
	}
	public Dan_Fu toZuShaoYangDan(){
		StaticLog.info("下手太阳，至小指之端。别入目锐眦，下足少阳，至小指次指之端");
		return Dan_Fu.getInstance();
	}
	/**
	 * 小肠的主要生理功能是主受盛化物和泌别清浊
	 */
	public DaChang_Fu xiaoChangabsorb(Pi_Zang pi,Wei_Fu wei){
		StaticLog.info("渣滓下传，而为粪溺，精华上奉，而变气血。");
		wei.createQi();
		pi.createXue(); 
		DaChang_Fu daChang = DaChang_Fu.getInstance();
		daChang.setMixture(new Mixture());
		return daChang;
	} 
	/**
	 * 单例模式保证唯一
	 */
	private XiaoChang_Fu() {} 
    private static class SingletonInstance {
        private static final XiaoChang_Fu INSTANCE = new XiaoChang_Fu();
    } 
    public static XiaoChang_Fu getInstance() {
        return SingletonInstance.INSTANCE;
    }
}
