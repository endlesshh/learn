package zhongyi.ren.fu;

import com.google.common.collect.Range;

import cn.hutool.log.StaticLog;
import zhongyi.Yang;
import zhongyi.ZangFu;
import zhongyi.wuxing.Huo;
/**
 * 三焦  三焦经
 * @author ShiQiang
 *三焦者，决渎之官，水道出焉
 *盖三焦之火秘，则上温脾胃而水道通；三焦之火泄，则下陷膀胱而水窍闭。
 */
public class SanJiao_Fu extends Huo implements ZangFu, Yang{
	public static int upDown = 1; 
	private Range<Integer> zi = Range.closedOpen(21, 23);
	
	public Dan_Fu f_21_23_t_Dan(){
		StaticLog.info("气血在三焦经的时间 从:{} -> {}",zi.lowerEndpoint(),zi.upperEndpoint());
		return Dan_Fu.getInstance();
	}
	
	public Wei_Fu toZuYangMingWei(){
		StaticLog.info("下足阳明，至中指之端。别入耳下，");
		return Wei_Fu.getInstance();
	}
	
	/**
	 * 单例模式保证唯一
	 */
	private SanJiao_Fu() {} 
    private static class SingletonInstance {
        private static final SanJiao_Fu INSTANCE = new SanJiao_Fu();
    } 
    public static SanJiao_Fu getInstance() {
        return SingletonInstance.INSTANCE;
    }
    /**
     * 上焦如雾
     * 及其已化，则气腾而上，盛于胸膈，故如雾露
     *
     */
    class ShangJiao{
    	
    }
    /**
     * 中焦如沤
     * 气水变化于中焦，沤者，气水方化，而未盛也
     *
     */
    class ZhongJiao{
    	
    }
    /**
     * 下焦如渎
     *水流而下，盛于膀胱，故如川渎
     *
     */
    class XiaJiao{
    	
    }
}
