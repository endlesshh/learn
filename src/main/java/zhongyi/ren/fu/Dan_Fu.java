package zhongyi.ren.fu;

import com.google.common.collect.Range;

import cn.hutool.log.StaticLog;
import zhongyi.TianGan;
import zhongyi.Yang;
import zhongyi.ZangFu;
import zhongyi.ren.zang.Gan_Zang;
import zhongyi.wuxing.Mu;
/**
 * 胆  胆经
 * @author ShiQiang
 *
 */
public class Dan_Fu extends Mu implements ZangFu, Yang{
	
	public static TianGan tGan = TianGan.jia;  
	
	public static int upDown = -1; 
	 
	private Range<Integer> zi = Range.closedOpen(23, 25); 
	public Gan_Zang f_23_1_t_Gan(){
		StaticLog.info("气血在胆经的时间 从:{} -> {}",zi.lowerEndpoint(),1);
		return Gan_Zang.getInstance();
	}
	
	public SanJiao_Fu toShouShaoYangSanJiao(){
		StaticLog.info("上循手少阳之分侧，下至名指之端。别入耳前，");
		return SanJiao_Fu.getInstance();
	}
	
	/**
	 * 单例模式保证唯一
	 */
	private Dan_Fu() {} 
    private static class SingletonInstance {
        private static final Dan_Fu INSTANCE = new Dan_Fu();
    } 
    public static Dan_Fu getInstance() {
        return SingletonInstance.INSTANCE;
    }
}
