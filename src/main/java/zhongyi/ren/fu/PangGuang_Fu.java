package zhongyi.ren.fu;

import com.google.common.collect.Range;

import cn.hutool.log.StaticLog;
import zhongyi.TianGan;
import zhongyi.Yang;
import zhongyi.ZangFu;
import zhongyi.other.drop.Pee;
import zhongyi.ren.zang.Shen_Zang;
import zhongyi.wuxing.Shui;
/**
 * 膀胱 膀胱经
 * @author ShiQiang
 *
 */
public class PangGuang_Fu extends Shui implements ZangFu, Yang{
	
	public static TianGan tGan = TianGan.ren;  
	
	public static int upDown = -1; 
	
	private Range<Integer> zi = Range.closedOpen(15, 17); 
	public Shen_Zang f_15_17_t_Sheng(){
		StaticLog.info("气血在膀胱经的时间 从:{} -> {}",zi.lowerEndpoint(),zi.upperEndpoint());
		return Shen_Zang.getInstance();
	}
	
	public XiaoChang_Fu toShouTaiYangXiaoChang(){ 
		StaticLog.info("常于平旦寅时，从足太阳之睛明始。睛明在目之内眦，足太阳之穴也。平旦阳气出于目，目张则气上行于头，循项，下足太阳，至小指之端。别入目内眦");
		return XiaoChang_Fu.getInstance();
	}
	
	
	private Pee pee;
	public void save(Pee pee){
		StaticLog.info("存储尿液");
		this.pee = pee;
	}
	public Pee getPee() {
		return pee;
	}
	public void setPee(Pee pee) {
		this.pee = pee;
	} 
	
	/**
	 * 单例模式保证唯一
	 */
	private PangGuang_Fu() {} 
    private static class SingletonInstance {
        private static final PangGuang_Fu INSTANCE = new PangGuang_Fu();
    } 
    public static PangGuang_Fu getInstance() {
        return SingletonInstance.INSTANCE;
    }
}
