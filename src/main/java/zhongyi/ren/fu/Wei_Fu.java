package zhongyi.ren.fu;

import java.util.List;

import com.google.common.collect.Range;

import cn.hutool.log.StaticLog;
import zhongyi.TianGan;
import zhongyi.Yang;
import zhongyi.ZangFu;
import zhongyi.other.eat.Edible;
import zhongyi.ren.jingshengqixue.Qi;
import zhongyi.ren.zang.Fei_Zang;
import zhongyi.ren.zang.Pi_Zang;
import zhongyi.ren.zang.Shen_Zang;
import zhongyi.wuxing.Tu;
/**
 * 胃  胃经
 * 
 * @author ShiQiang
 *
 */
public class Wei_Fu extends Tu implements ZangFu, Yang{
	
	public static TianGan tGan = TianGan.wu;  
	
	public static int upDown = -1;
	/**
	 * 胃以纯阳而含阴气 - 浊阴下降，是以清虚而善容纳
	 * 自己理解 -- 胃恒温内清 -> 腑通，温度低，
	 */ 
	public int yin = super.yin;
	
	/**
	 * 与为互为表里
	 */
	private Pi_Zang pi;
	
	private List<Edible> eats;
	
	public Pi_Zang eat(List<Edible> food){
		StaticLog.info("水谷入胃");
		this.eats = food;
		//调用脾脏
		pi = Pi_Zang.getInstance();
		pi.setWei(this);
		return pi;
	}
	/**
	 * 胃为化气之原
	 * @return
	 */
	public void createQi(){
		StaticLog.info("胃为化气之原");
		StaticLog.info("食物随阴而降：{}",yin);
		Fei_Zang.getInstance().store(Qi.getInstance()); 
	} 
	
	private Range<Integer> zi = Range.closedOpen(7, 9); 
	public Pi_Zang f_7_9_t_Pi(){
		StaticLog.info("气血在胃经的时间 从:{} -> {}",zi.lowerEndpoint(),zi.upperEndpoint());
		return Pi_Zang.getInstance();
	}
	
	public Object toShouYangMingDaChang(int i){
		StaticLog.info("循环完成第"+i+"周");
		if(i <= 25){
			StaticLog.info("别入耳下，下手阳明，至次指之端。其至于足也，入足心，出内踝，下入足少阴经。阴蹻者，足少阴之别，属于目内眦。自阴蹻而复合于目，交于足太阳之睛明");
			return PangGuang_Fu.getInstance();
		}else{
			StaticLog.info("日入阳尽，而阴受气矣，于是内入于阴藏，（从阳跷脉）其入于阴也，常从足少阴之经而注于肾");
			return Shen_Zang.getInstance();
		}
		
	}
	
	/**
	 * 单例模式保证唯一
	 */ 
	private Wei_Fu(){} 
    private static class SingletonInstance { 
        private static final Wei_Fu INSTANCE = new Wei_Fu();
    } 
    public static Wei_Fu getInstance() {  
        return SingletonInstance.INSTANCE;
    }
	public List<Edible> getEats() {
		return eats;
	}
	public void setEats(List<Edible> eats) {
		this.eats = eats;
	}
	public Pi_Zang getPi() {
		return pi;
	}
	public void setPi(Pi_Zang pi) {
		this.pi = pi;
	}
}
