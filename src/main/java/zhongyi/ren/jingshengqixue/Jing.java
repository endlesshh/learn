package zhongyi.ren.jingshengqixue;

import zhongyi.Names;
import zhongyi.Yin;
import zhongyi.ren.zang.Fei_Zang;
import zhongyi.ren.zang.Shen_Zang;

/**
 * 精藏于肾
 * @author ShiQiang
 * @param <T>
 *
 */
public class Jing<T> implements Yin{
	
	public String name(T object){
		if(object instanceof Shen_Zang){
			return Names.Jing.getName();
		}
		if(object instanceof Fei_Zang){
			return Names.Jing.getNextName();
		}
		return Names.Xue.getName();
	}
	
	public static void main(String[] args) {
		Jing<Object> t = new Jing<Object>();
		Shen_Zang jyj = Shen_Zang.getInstance();
		System.out.println(t.name(jyj));
		Fei_Zang gan = Fei_Zang.getInstance();
		System.out.println(t.name(gan));
	}
	/**
	 * 单例模式保证唯一
	 */
	private Jing() {} 
    private static class SingletonInstance {
        private static final Jing<?> INSTANCE = new Jing<>();
    } 
    public static Jing<?> getInstance() {
        return SingletonInstance.INSTANCE;
    }
}
