package zhongyi.ren.jingshengqixue;

import zhongyi.Names;
import zhongyi.Yang;
import zhongyi.ZangFu;
import zhongyi.ren.liujing.JueYinJing;
import zhongyi.ren.zang.Gan_Zang;

/**
 * 气 - 气为血之帅
 * 在脏腑则曰气，而在经络则为卫
 * @author ShiQiang
 * @param <T>
 *
 */
public class Qi<T> implements Yang{
	
	public String name(T object){
		if(object instanceof ZangFu){
			return Names.Qi.getName();
		}else{
			return Names.Qi.getNextName();
		}
	}
	
	public static void main(String[] args) {
		Qi<Object> t = new Qi<Object>();
		JueYinJing jyj = new JueYinJing();
		System.out.println(t.name(jyj));
		Gan_Zang gan = Gan_Zang.getInstance();
		System.out.println(t.name(gan));
	}
	/**
	 * 单例模式保证唯一
	 */
	private Qi() {} 
    private static class SingletonInstance {
        private static final Qi<?> INSTANCE = new Qi<>();
    } 
    public static Qi<?> getInstance() {
        return SingletonInstance.INSTANCE;
    }
}
