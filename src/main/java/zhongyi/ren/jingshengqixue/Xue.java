package zhongyi.ren.jingshengqixue;

import zhongyi.Names;
import zhongyi.Yin;
import zhongyi.ZangFu;

/**
 * 血 - 血为气之母
 * 脏腑则曰血，而在经络则为营
 * @author ShiQiang
 * @param <T>
 *
 */
public class Xue<T> implements Yin{
	
	public String name(T object){
		if(object instanceof ZangFu){
			return Names.Xue.getName();
		}else{
			return Names.Xue.getNextName();
		}
	}
	
	/**
	 * 单例模式保证唯一
	 */
	private Xue() {} 
    private static class SingletonInstance {
        private static final Xue<?> INSTANCE = new Xue<>();
    } 
    public static Xue<?> getInstance() {
        return SingletonInstance.INSTANCE;
    }
}
