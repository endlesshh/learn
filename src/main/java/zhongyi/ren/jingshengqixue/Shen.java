package zhongyi.ren.jingshengqixue;

import cn.hutool.log.StaticLog;
import zhongyi.Names;
import zhongyi.Yang;
import zhongyi.ren.five.WuZhi;
import zhongyi.ren.liujing.JueYinJing;
import zhongyi.ren.zang.Gan_Zang;
import zhongyi.ren.zang.Xin_Zang;

/**
 * 神发于心
 * @author ShiQiang
 * @param <T>
 *
 */
public class Shen<T> implements Yang{
	
	public String name(T object){
		if(object instanceof Xin_Zang){
			return Names.Shen.getName();
		}
		if(object instanceof Gan_Zang){
			return Names.Shen.getNextName();
		}
		return Names.Qi.getName();
	}
	
	/**
	 * 心主五志 - 心的神主五志
	 * @return
	 */
	
	public String feeling(Object ob){
		String yanSe = WuZhi.get(ob);
		StaticLog.info("脏的志:{}",yanSe);
		return yanSe;
	}
	
	public static void main(String[] args) {
		Shen<Object> t = new Shen<Object>();
		JueYinJing jyj = new JueYinJing();
		System.out.println(t.name(jyj));
		Gan_Zang gan = Gan_Zang.getInstance();
		System.out.println(t.name(gan));
	}
	/**
	 * 单例模式保证唯一
	 */
	private Shen() {} 
    private static class SingletonInstance {
        private static final Shen<?> INSTANCE = new Shen<>();
    } 
    public static Shen<?> getInstance() {
        return SingletonInstance.INSTANCE;
    }
}
