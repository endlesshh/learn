package zhongyi.ren.five;

import zhongyi.ren.zang.Fei_Zang;
import zhongyi.ren.zang.Gan_Zang;
import zhongyi.ren.zang.Shen_Zang;
import zhongyi.ren.zang.Xin_Zang;

/**
 * 味
 * @author Administrator
 *
 */
public enum WuWei {
	
	Suan("酸"), Ku("苦"), Gan("甘"), Xin("辛"), Xian("咸"); 
	
	private String name;
	
	private WuWei(String name){
		this.name = name;
	}
	
	public static String get(Object wuZang){
		if(wuZang instanceof Gan_Zang){
			return WuWei.Suan.getName();
		}else if(wuZang instanceof Xin_Zang){
			return WuWei.Ku.getName();
		}else if(wuZang instanceof Fei_Zang){
			return WuWei.Xin.getName();
		}else if(wuZang instanceof Shen_Zang){
			return WuWei.Xian.getName();
		}else{
			return WuWei.Gan.getName();
		} 
	} 
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
