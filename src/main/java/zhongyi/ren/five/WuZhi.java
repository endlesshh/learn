package zhongyi.ren.five;

import zhongyi.ren.zang.Fei_Zang;
import zhongyi.ren.zang.Gan_Zang;
import zhongyi.ren.zang.Shen_Zang;
import zhongyi.ren.zang.Xin_Zang;
/**
 * 情
 * @author ShiQiang
 *
 */
public enum WuZhi {
	
	Nu("怒"), Xi("喜"), Bei("悲"), Kong("恐"), Si("思"); 
	
	private String name;
	
	private WuZhi(String name){
		this.name = name;
	}
	
	public static String get(Object wuZang){
		if(wuZang instanceof Gan_Zang){
			return WuZhi.Nu.getName();
		}else if(wuZang instanceof Xin_Zang){
			return WuZhi.Xi.getName();
		}else if(wuZang instanceof Fei_Zang){
			return WuZhi.Bei.getName();
		}else if(wuZang instanceof Shen_Zang){
			return WuZhi.Kong.getName();
		}else{
			return WuZhi.Si.getName();
		} 
	} 
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
