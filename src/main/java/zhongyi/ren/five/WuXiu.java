package zhongyi.ren.five;

import zhongyi.ren.zang.Fei_Zang;
import zhongyi.ren.zang.Gan_Zang;
import zhongyi.ren.zang.Shen_Zang;
import zhongyi.ren.zang.Xin_Zang;

/**
 * 臭
 * @author ShiQiang
 *
 */
public enum WuXiu {
	
	Zao("臊"), Jiao("焦"), Xiang("香"), Xing("腥"), Fu("腐"); 
	
	private String name;
	
	private WuXiu(String name){
		this.name = name;
	}
	
	public static String get(Object wuZang){
		if(wuZang instanceof Gan_Zang){
			return WuXiu.Zao.getName();
		}else if(wuZang instanceof Xin_Zang){
			return WuXiu.Jiao.getName();
		}else if(wuZang instanceof Fei_Zang){
			return WuXiu.Xing.getName();
		}else if(wuZang instanceof Shen_Zang){
			return WuXiu.Fu.getName();
		}else{
			return WuXiu.Xiang.getName();
		} 
	} 
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
