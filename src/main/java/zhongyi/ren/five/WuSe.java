package zhongyi.ren.five;

import zhongyi.ren.zang.Fei_Zang;
import zhongyi.ren.zang.Gan_Zang;
import zhongyi.ren.zang.Shen_Zang;
import zhongyi.ren.zang.Xin_Zang;
/**
 * 色
 * @author ShiQiang
 *
 */
public enum WuSe {
	
	Qing("青"), Chi("赤"), Huang("黄"), Bai("白"), Hei("黑"); 
	
	private String name;
	
	private WuSe(String name){
		this.name = name;
	}
	
	public static String get(Object wuZang){
		if(wuZang instanceof Gan_Zang){
			return WuSe.Qing.getName();
		}else if(wuZang instanceof Xin_Zang){
			return WuSe.Chi.getName();
		}else if(wuZang instanceof Fei_Zang){
			return WuSe.Bai.getName();
		}else if(wuZang instanceof Shen_Zang){
			return WuSe.Hei.getName();
		}else{
			return WuSe.Huang.getName();
		} 
	} 
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
