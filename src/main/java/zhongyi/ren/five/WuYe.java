package zhongyi.ren.five;

import zhongyi.ren.zang.Fei_Zang;
import zhongyi.ren.zang.Gan_Zang;
import zhongyi.ren.zang.Shen_Zang;
import zhongyi.ren.zang.Xin_Zang;

/**
 * 液
 * @author ShiQiang
 * 
 */
public enum WuYe {

	Lei("泪"), Han("汗"), Xian("涎"), Ti("涕"), Tuo("唾"); 
	
	private String name;
	
	private WuYe(String name){
		this.name = name;
	}
	
	public static String get(Object wuZang){
		if(wuZang instanceof Gan_Zang){
			return WuYe.Lei.getName();
		}else if(wuZang instanceof Xin_Zang){
			return WuYe.Han.getName();
		}else if(wuZang instanceof Fei_Zang){
			return WuYe.Ti.getName();
		}else if(wuZang instanceof Shen_Zang){
			return WuYe.Tuo.getName();
		}else{
			return WuYe.Xian.getName();
		} 
	} 
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
