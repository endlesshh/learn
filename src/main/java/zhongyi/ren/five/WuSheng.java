package zhongyi.ren.five;

import zhongyi.ren.zang.Fei_Zang;
import zhongyi.ren.zang.Gan_Zang;
import zhongyi.ren.zang.Shen_Zang;
import zhongyi.ren.zang.Xin_Zang;

/**
 * 声
 * @author ShiQiang
 *
 */
public enum WuSheng {
	
	Hu("呼"), Yan("言"), Ge("歌"), Ku("哭"), Shen("呻"); 
	
	private String name;
	
	private WuSheng(String name){
		this.name = name;
	}
	
	public static String get(Object wuZang){
		if(wuZang instanceof Gan_Zang){
			return WuSheng.Hu.getName();
		}else if(wuZang instanceof Xin_Zang){
			return WuSheng.Yan.getName();
		}else if(wuZang instanceof Fei_Zang){
			return WuSheng.Ku.getName();
		}else if(wuZang instanceof Shen_Zang){
			return WuSheng.Shen.getName();
		}else{
			return WuSheng.Ge.getName();
		} 
	} 
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
