package zhongyi.ren.liujing;

import cn.hutool.log.StaticLog;
import zhongyi.JingQi;
import zhongyi.Yang;
import zhongyi.liuqi.ZaoQi;
import zhongyi.ren.fu.DaChang_Fu;
import zhongyi.ren.fu.Wei_Fu;
import zhongyi.wuxing.Jin;

/**
 * 阳明 - 燥金
 * @author ShiQiang
 *
 */
public class YangMingJing extends Jin implements ZaoQi, JingQi, Yang{
	
	public DaChang_Fu shou(){
		return DaChang_Fu.getInstance();
	}
	
	public Wei_Fu zu(){
		return Wei_Fu.getInstance();
	}
	
	public DaChang_Fu zhu(){
		StaticLog.info("手阳明大肠");
		return shou();
	} 
}
