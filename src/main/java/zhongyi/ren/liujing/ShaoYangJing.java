package zhongyi.ren.liujing;

import cn.hutool.log.StaticLog;
import zhongyi.JingQi;
import zhongyi.Yang;
import zhongyi.liuqi.ShuQi;
import zhongyi.ren.fu.Dan_Fu;
import zhongyi.ren.fu.SanJiao_Fu;
import zhongyi.wuxing.Huo;

/**
 * 少阳 - 相火
 * @author ShiQiang
 *
 */
public class ShaoYangJing extends Huo implements ShuQi, JingQi, Yang{  
	
	public SanJiao_Fu shou(){
		return SanJiao_Fu.getInstance();
	}
	
	public Dan_Fu zu(){
		return Dan_Fu.getInstance();
	}
	
	public SanJiao_Fu zhu(){
		StaticLog.info("手少阳三焦");
		return shou();
	} 
}
