package zhongyi.ren.liujing;

import cn.hutool.log.StaticLog;
import zhongyi.JingQi;
import zhongyi.Yang;
import zhongyi.liuqi.HanQi;
import zhongyi.ren.fu.PangGuang_Fu;
import zhongyi.ren.fu.XiaoChang_Fu;
import zhongyi.wuxing.Shui;

/**
 * 太阳 - 寒水
 * @author ShiQiang
 *
 */
public class TaiYangJing extends Shui implements HanQi, JingQi, Yang{

	public XiaoChang_Fu shou(){
		return XiaoChang_Fu.getInstance();
	}
	
	public PangGuang_Fu zu(){
		return PangGuang_Fu.getInstance();
	}
	
	public PangGuang_Fu zhu(){
		StaticLog.info("足太阳膀胱");
		return zu();
	} 
}
