package zhongyi.ren.liujing;

import cn.hutool.log.StaticLog;
import zhongyi.JingQi;
import zhongyi.Yin;
import zhongyi.liuqi.FengQi;
import zhongyi.ren.zang.Gan_Zang;
import zhongyi.ren.zang.Pi_Zang;
import zhongyi.ren.zang.Shen_Zang;
import zhongyi.ren.zang.XinBao_Zang;
import zhongyi.wuxing.Mu;

/**
 * 厥阴 - 风木
 * 主 从 化
 * @author ShiQiang
 *
 */
public class JueYinJing extends Mu implements FengQi, JingQi, Yin{ 
	/**
	 * 生于肾水(一滴精十滴血)
	 */
	private Shen_Zang shen;
	/**
	 * 长于脾土(脾为生血之源)
	 */
	private Pi_Zang pi;
	/**
	 * 在天为风，在地为木，在人为肝
	 */
	private Gan_Zang gan;
	
	
	public XinBao_Zang shou(){
		return XinBao_Zang.getInstance();
	}
	
	public Gan_Zang zu(){
		return Gan_Zang.getInstance();
	}
	
	public Gan_Zang zhu(){
		StaticLog.info("足厥阴肝");
		return zu();
	} 
	@Override
	public String toString() {
		return super.toString();
	}
}
