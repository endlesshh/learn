package zhongyi.ren.liujing;

import cn.hutool.log.StaticLog;
import zhongyi.JingQi;
import zhongyi.Yin;
import zhongyi.liuqi.ShiQi;
import zhongyi.ren.zang.Fei_Zang;
import zhongyi.ren.zang.Pi_Zang;
import zhongyi.wuxing.Tu;

/**
 * 太阴 - 湿土
 * @author ShiQiang
 *
 */
public class TaiYinJing extends Tu implements ShiQi, JingQi, Yin{

	public Fei_Zang shou(){
		return Fei_Zang.getInstance();
	}
	
	public Pi_Zang zu(){
		return Pi_Zang.getInstance();
	}
	
	public Pi_Zang zhu(){
		StaticLog.info("足太阴脾");
		return zu();
	} 
}
