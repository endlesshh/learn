package zhongyi.ren.liujing;

import cn.hutool.log.StaticLog;
import zhongyi.JingQi;
import zhongyi.Yin;
import zhongyi.liuqi.ReQi;
import zhongyi.ren.zang.Shen_Zang;
import zhongyi.ren.zang.Xin_Zang;
import zhongyi.wuxing.Huo;

/**
 * 少阴 - 君火
 * @author ShiQiang
 *
 */
public class ShaoYinJing extends Huo implements ReQi, JingQi, Yin{
	
	public Xin_Zang shou(){
		return Xin_Zang.getInstance();
	}
	
	public Shen_Zang zu(){
		return Shen_Zang.getInstance();
	}
	
	public Xin_Zang zhu(){
		StaticLog.info("手少阴心");
		return shou();
	} 
}
