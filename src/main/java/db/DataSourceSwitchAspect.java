package db;


import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @ClassName DataSourceSwitchAspect
 * @Author hailong.zheng
 * @Date 2018/11/29 11:03
 * @Version v1.0
 */
@Component
@Order(value = -100)
@Aspect
public class DataSourceSwitchAspect {
    private static Logger log = LoggerFactory.getLogger(DataSourceSwitchAspect.class);

    @Pointcut("execution(* com.ifast.api.controller.*.*(..))")
    private void db1Aspect() {
    }

    @Pointcut("execution(* com.ifast.zhengwu.controller.*.*(..))")
    private void db3Aspect() {
    }

    
    @Pointcut("execution(* com.ifast.*.service..*.*(..))")
    private void db2Aspect() {
    }


    @Before("db1Aspect()")
    public void db1() {
        log.info("切换到sqlServer数据源");
        DbContextHolder.setDbType(DBTypeEnum.db1);
    }
    
    @Before("db3Aspect()")
    public void db3() {
        log.info("切换到sqlServer数据源");
        DbContextHolder.setDbType(DBTypeEnum.db1);
    }
    
    @Before("db2Aspect()")
    public void db2() {
        log.info("切换到oracle数据源");
        DbContextHolder.setDbType(DBTypeEnum.db2);
    } 
}