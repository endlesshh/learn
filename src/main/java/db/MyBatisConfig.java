package db;

//import java.util.HashMap;
//import java.util.Map;
//
//import javax.sql.DataSource;
//
//import org.apache.ibatis.plugin.Interceptor;
//import org.apache.ibatis.session.SqlSessionFactory;
//import org.apache.ibatis.type.JdbcType;
//import org.mybatis.spring.annotation.MapperScan;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
//import org.springframework.core.io.support.ResourcePatternResolver;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
//import com.baomidou.mybatisplus.MybatisConfiguration;
//import com.baomidou.mybatisplus.entity.GlobalConfiguration;
//import com.baomidou.mybatisplus.mapper.LogicSqlInjector;
//import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
//import com.baomidou.mybatisplus.spring.MybatisSqlSessionFactoryBean;
//import com.ifast.common.mybatiscomm.MyMetaObjectHandler;
//
//
//@EnableTransactionManagement
//@Configuration
//@MapperScan({"com.ifast.*.dao","com.luculent.*.dao"})
//public class MyBatisConfig {
//
// 
//    @Bean(name = "sqlServer")
//    @ConfigurationProperties(prefix = "spring.datasource.druid.primary")
//    public DataSource db1() {
//        return DruidDataSourceBuilder.create().build();
//    }
//    
//    @Bean(name = "oracle")
//    @ConfigurationProperties(prefix = "spring.datasource.druid.secondary")
//    public DataSource db2() {
//        return DruidDataSourceBuilder.create().build();
//    }
//
//    /**
//     * 动态数据源配置
//     *
//     * @return
//     */
//    @Bean
//    @Primary
//    public DataSource multipleDataSource(DataSource db1,
//                                         DataSource db2) {
//        DynamicDataSource dynamicDataSource = new DynamicDataSource();
//        Map<Object, Object> targetDataSources = new HashMap<>();
//        targetDataSources.put(DBTypeEnum.db1.getValue(), db1);
//        targetDataSources.put(DBTypeEnum.db2.getValue(), db2);
//        dynamicDataSource.setTargetDataSources(targetDataSources);
//        dynamicDataSource.setDefaultTargetDataSource(db2);
//        return dynamicDataSource;
//    }
//
//    @Bean("sqlSessionFactory")
//    public SqlSessionFactory sqlSessionFactory(@Qualifier("sqlServer") DataSource db1,
//            @Qualifier("oracle") DataSource db2) throws Exception {
//        MybatisSqlSessionFactoryBean sqlSessionFactory = new MybatisSqlSessionFactoryBean();
//        sqlSessionFactory.setDataSource(multipleDataSource(db1, db2));
//        // 设置mybatis的主配置文件
//        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
//        sqlSessionFactory.setMapperLocations( resolver.getResources("classpath*:/mapper/**/*Mapper.xml"));
//        // 设置别名包
//        //sqlSessionFactory.setTypeAliasesPackage("com.ifast.**.domain");
//
//        MybatisConfiguration configuration = new MybatisConfiguration();
//        configuration.setJdbcTypeForNull(JdbcType.NULL);
//        configuration.setMapUnderscoreToCamelCase(false);
//        configuration.setCacheEnabled(false);
//        sqlSessionFactory.setConfiguration(configuration);
//        sqlSessionFactory.setGlobalConfig(globalConfiguration());
//        //添加分页功能
//        sqlSessionFactory.setPlugins(new Interceptor[]{
//                paginationInterceptor()
//        });
//        return sqlSessionFactory.getObject();
//    }
//
//    /**
//     * 分页插件
//     * 
//     * @return
//     * @author zhongweiyuan
//     * @date 2018年4月14日下午4:13:15
//     */
//    @Bean
//    public PaginationInterceptor paginationInterceptor() {
//        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
//        return paginationInterceptor;
//    }
//
//    /**
//     * mybatisPlus全局配置
//     * @return
//     */
//    @Bean
//    @ConfigurationProperties(prefix = "mybatis-plus")
//    public GlobalConfiguration globalConfiguration() { 
//    	GlobalConfiguration conf = new GlobalConfiguration(new LogicSqlInjector());
//    	conf.setMetaObjectHandler(new MyMetaObjectHandler());
//    	return conf;
//    }
//}
