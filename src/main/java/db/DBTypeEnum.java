package db;

/**
 * @ClassName DBTypeEnum
 * @Author hailong.zheng
 * @Date 2018/11/29 10:59
 * @Version v1.0
 */
public enum DBTypeEnum {

    db1("sqlServer"), db2("oracle");
	
    private String value;

    DBTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}