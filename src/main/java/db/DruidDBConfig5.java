/*
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

*//**
 * Created by secondaryKey on 17/2/4.
 *//*
@Configuration()
//secondary
public class DruidDBConfig5 {
    private Logger logger = LoggerFactory.getLogger(DruidDBConfig5.class);
    @Autowired(required = false)
    @Value("${spring.datasource.dynamic.datasource.secondary.url}")
    private String dbUrl;

    @Autowired(required = false)
    @Value("${spring.datasource.dynamic.datasource.secondary.username}")
    private String username;

    @Autowired(required = false)
    @Value("${spring.datasource.dynamic.datasource.secondary.password}")
    private String password;

    @Autowired(required = false)
    @Value("${spring.datasource.dynamic.datasource.secondary.driverClassName}")
    private String driverClassName;

    @Autowired(required = false)
    @Value("${spring.datasource.dynamic.datasource.secondary.initialSize}")
    private int initialSize;

    @Autowired(required = false)
    @Value("${spring.datasource.dynamic.datasource.secondary.minIdle}")
    private int minIdle;

    @Autowired(required = false)
    @Value("${spring.datasource.dynamic.datasource.secondary.maxActive}")
    private int maxActive;

    @Autowired(required = false)
    @Value("${spring.datasource.dynamic.datasource.secondary.maxWait}")
    private int maxWait;

    @Autowired(required = false)
    @Value("${spring.datasource.dynamic.datasource.secondary.timeBetweenEvictionRunsMillis}")
    private int timeBetweenEvictionRunsMillis;

    @Autowired(required = false)
    @Value("${spring.datasource.dynamic.datasource.secondary.minEvictableIdleTimeMillis}")
    private int minEvictableIdleTimeMillis;

    @Autowired(required = false)
    @Value("${spring.datasource.dynamic.datasource.secondary.validationQuery}")
    private String validationQuery;

    @Autowired(required = false)
    @Value("${spring.datasource.dynamic.datasource.secondary.testWhileIdle}")
    private boolean testWhileIdle;

    @Autowired(required = false)
    @Value("${spring.datasource.dynamic.datasource.secondary.testOnBorrow}")
    private boolean testOnBorrow;

    @Autowired(required = false)
    @Value("${spring.datasource.dynamic.datasource.secondary.testOnReturn}")
    private boolean testOnReturn;

    @Autowired(required = false)
    @Value("${spring.datasource.dynamic.datasource.secondary.poolPreparedStatements}")
    private boolean poolPreparedStatements;

    @Autowired(required = false)
    @Value("${spring.datasource.dynamic.datasource.secondary.maxPoolPreparedStatementPerConnectionSize}")
    private int maxPoolPreparedStatementPerConnectionSize;

    @Autowired(required = false)
    @Value("${spring.datasource.dynamic.datasource.secondary.filters}")
    private String filters;

    @Autowired(required = false)
    @Value("{spring.datasource.dynamic.datasource.secondary.connectionProperties}")
    private String connectionProperties;

    @Bean(name="secondary") // 声明其为Bean实例
    @Qualifier("secondary")
    public DataSource dataSource() {
//        DruidDataSource datasource = new DruidDataSource();
//
//        datasource.setUrl(this.dbUrl);
//        datasource.setUsername(username);
//        datasource.setPassword(password);
//        datasource.setDriverClassName(driverClassName);
//
//        // configuration
//        datasource.setInitialSize(initialSize);
//        datasource.setMinIdle(minIdle);
//        datasource.setMaxActive(maxActive);
//        datasource.setMaxWait(maxWait);
//        datasource.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
//        datasource.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
//        datasource.setValidationQuery(validationQuery);
//        datasource.setTestWhileIdle(testWhileIdle);
//        datasource.setTestOnBorrow(testOnBorrow);
//        datasource.setTestOnReturn(testOnReturn);
//        datasource.setPoolPreparedStatements(poolPreparedStatements);
//        datasource.setMaxPoolPreparedStatementPerConnectionSize(maxPoolPreparedStatementPerConnectionSize);
//        try {
//            datasource.setFilters(filters);
//        } catch (SQLException e) {
//            logger.error("druid configuration initialization filter", e);
//        }
//        datasource.setConnectionProperties(connectionProperties);
//
//        return datasource;
    }

    @Bean
    public ServletRegistrationBean druidServlet() {
        ServletRegistrationBean reg = new ServletRegistrationBean();
       // reg.setServlet(new StatViewServlet());
        reg.addUrlMappings("/druid/*");
        reg.addInitParameter("allow", ""); // 白名单
        return reg;
    }

    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
       // filterRegistrationBean.setFilter(new WebStatFilter());
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
        filterRegistrationBean.addInitParameter("profileEnable", "true");
        filterRegistrationBean.addInitParameter("principalCookieName", "USER_COOKIE");
        filterRegistrationBean.addInitParameter("principalSessionName", "USER_SESSION");
        filterRegistrationBean.addInitParameter("DruidWebStatFilter", "/*");
        return filterRegistrationBean;
    }
}
*/