package db;

/**
 * @ClassName DbContextHolder
 * @Author hailong.zheng
 * @Date 2018/11/29 11:01
 * @Version v1.0
 */
public class DbContextHolder {

    private static final ThreadLocal<String> contextHolder = new ThreadLocal<>();
    /**
     * 设置数据源
     * @param dbTypeEnum
     */
    public static void setDbType(DBTypeEnum dbTypeEnum) { 
        contextHolder.set(dbTypeEnum.getValue());

    }

    /**
     * 取得当前数据源
     * @return
     */
    public static String getDbType() { 
        return (String) contextHolder.get();
    }

    /**
     * 清除上下文数据
     */
    public static void clearDbType() {
        contextHolder.remove();
    }
}
