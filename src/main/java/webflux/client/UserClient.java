package webflux.client;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import webflux.model.User;

public class UserClient {

	public static void main(String[] args) {

		//
		ClientResponse response = WebClient.create("http://localhost:9000").get().uri("/api/user")
				.accept(MediaType.APPLICATION_JSON).exchange().block();
		
		System.out.println( response.statusCode().value() == 200);
		List<User> users = response.bodyToFlux(User.class).collectList().block();
		System.out.println( users.size() == 2);
		System.out.println( users.iterator().next().getUser().equals("User1"));

		//
		User user = WebClient.create("http://localhost:9000").get().uri("/api/user/1")
				.accept(MediaType.APPLICATION_JSON).exchange().flatMap(resp -> resp.bodyToMono(User.class)).block();
		System.out.println( user.getId() == 1);
		System.out.println( user.getUser().equals("User1"));

		response = WebClient.create("http://localhost:9000").get().uri("/api/user/10")
				.accept(MediaType.APPLICATION_JSON).exchange().block();
		System.out.println( response.statusCode().value() == 404);

	}

}
