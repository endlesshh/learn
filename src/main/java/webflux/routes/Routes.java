package webflux.routes;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;

import webflux.handler.UserHandler;

@Configuration
public class Routes {
	@Autowired
	private UserHandler userHandler; 
	
	@Bean
	public RouterFunction<?> routerFunction() {
		return
				route(GET("/api/users").and(accept(MediaType.APPLICATION_JSON)), userHandler::handleGetUsers)
				.and(route(GET("/api/users/{id}").and(accept(MediaType.APPLICATION_JSON)), userHandler::handleGetUserById));
//				.and(route(POST("/users"), userHandler::handleGetUsers));
		
	}

}
