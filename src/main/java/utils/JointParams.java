package utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.experimental.ExtensionMethod;

/**
 * 拼接查询条件
 * 只拼接 Map<String,String>和Map<String,List<String>>
 * @author ShiQiang
 *
 */
@Builder
@AllArgsConstructor
@ExtensionMethod({ExtensionsTool.class})
public class JointParams { 
	
	@Builder.Default
	private List<String> urls = Lists.newArrayList();

	/**
	 * 设置请求的基本路径
	 * 
	 * @param basePath
	 * @return
	 */
	public JointParams base(String basePath) {
		if (!basePath.isNull()) {
			urls.add(basePath);
		} 
		return this;
	}

	/**
	 * 获取最终的查询请求
	 * 
	 * @return
	 */
	public List<String> urls() {
		//return urls.stream().map(url -> StringUtils.removeFirst(url, "&")).collect(Collectors.toList());
		return urls;
	}

	/**
	 * 多个查询条件
	 * 
	 * @param cols
	 * @return
	 */
	private JointParams joinCols(Map<String, String> cols) {

		if (urls.isNull()) {
			throw new RuntimeException("使用 base(String)请设置请求基本路径");
		}
		if (cols.isNull()) {
			throw new RuntimeException("缺少要拼接的值");
		}
		Function<String, String> deal = url -> {
			String newUrl = "";
			for (String key : cols.keySet()) {
				newUrl += "&" + key + "=" + cols.get(key);
			}
			return url + newUrl;
		};
		urls = urls.stream().map(deal).collect(Collectors.toList());
		return this;
	}

	private List<String> kMoreV(String key, List<String> values) {
		List<String> kvs = Lists.newArrayList();
		values.stream().forEach(value -> {
			kvs.add(key.concat("=").concat(value));
		});
		return kvs;
	}

	private List<String> kMoreV(List<String> urls, Map<String, List<String>> rows) {
		if (rows.isNull()) {
			return urls;
		}
		List<String> keys = Lists.newArrayList(rows.keySet());
		String key = keys.get(0);
		List<String> ve = rows.get(key);

		if (ve.isNull()) {
			throw new RuntimeException(key.concat(":缺少需要拼接的值"));
		}

		List<String> pinList = Lists.newArrayList();
		urls.stream().forEach(url -> {
			kMoreV(key, ve).stream().forEach(p -> {
				pinList.add(url.concat("&").concat(p));
			});
		});
		urls.clear();
		urls = pinList;
		rows.remove(key);
		return kMoreV(urls, rows);
	}

	/**
	 * 一个key多个value的情况
	 * 
	 * @param rows
	 * @return
	 */
	private JointParams joinRows(Map<String, List<String>> rows) {
		if (urls.isNull()) {
			throw new RuntimeException("使用 base(String)设置请求基本路径");
		}
		if (rows.isNull()) {
			throw new RuntimeException("缺少要拼接的值");
		}
		urls = kMoreV(urls, rows);
		return this;
	}
	/**
	 * 只接受 Map<String,String>和Map<String,List<String>>
	 * @param map
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public JointParams join(Map<String,Object> map) {
		if (urls.isNull()) {
			throw new RuntimeException("使用 base(String)设置请求基本路径");
		}
		if (map.isNull()) {
			throw new RuntimeException("缺少要拼接的值");
		}
		map.forEach(new BiConsumer<String,Object>() { 
			@Override
			public void accept(String t, Object u) {
				if(u instanceof String){ 
					joinCols(this.newHashMap(t,String.valueOf(u)));
				}else if(u instanceof List){ 
					joinRows(this.newHashMap(t,(List<String>)u));
				} 
			} 
		}); 
		return this;
	} 
	
	public static void main(String[] args) {
		Map<String,Object> mapList = new HashMap<String,Object>();
		List<String> a = new ArrayList<String>();
		a.add("2018");
		a.add("2017");
		// a.add ("2016");
		// a.add ("2015");
		// a.add ("2014");
		mapList.put("ND", a);
		 List<String> c = new ArrayList<String> ();
		 c.add ("150402000");
		// // c.add ("150402001");
		//// c.add ("150402002");
		//// c.add ("150402003");
		//// c.add ("150402004");
		mapList.put ("SZXIAN",c);

		// System.out.println(kMoreV(Lists.newArrayList("http://www.baidu.com"),mapList));
		mapList.put("HBH", "12133");
		mapList.put("RBH", "222222");
		JointParams join = JointParams.builder().build();
	   
		join.base("http://219.159.44.169:32201/fupinnew").join(mapList);
		System.out.println(join.urls());
	}
}
