package utils.genecode;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValue;

import cn.hutool.log.StaticLog;
import lombok.experimental.ExtensionMethod;
import utils.genecode.config.ExecuteModel;
import utils.genecode.constant.Globals;
import utils.genecode.generate.IGenerator;
import utils.genecode.generate.factory.CodeFactory;
import utils.genecode.util.CodeDateUtils;
import utils.genecode.util.CodeResourceUtil;

/**
 * 
 * @Description:代码生成器
 * @Author: ShiQiang
 * @Since:2017年5月18日下午2:47:08
 */

@ExtensionMethod({ CodeDateUtils.class })
public class CodeGenerate implements IGenerator {
	// 项目的名称路径 如：com.taobao
	private static String projectName = CodeResourceUtil.bussiPackage;
	// 单位的全称 fupinban
	private static String fullBussiName = CodeResourceUtil.danweifullname;
	// 单位简称 FPB
	private static String bussiName = CodeResourceUtil.danweisimplename;
	// 项目说明
	private static String ftlDescription = CodeResourceUtil.description;

	public static final Config path = ConfigFactory.load("generatorcode/luculent_template.conf").getConfig("TPATH");

	public Map<String, Object> execute() {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put(Globals.projectName, projectName);
		data.put(Globals.fullBussiName, fullBussiName);
		data.put(Globals.bussiName, bussiName);
		data.put(Globals.ftlDescription, ftlDescription);
		Date date = new Date();
		data.put(Globals.ftlCreateTime, date.dateToString());
		return data;
	}

	/**
	 * 生成文件
	 */
	public void generateToFile() throws Exception {
		StaticLog.info("----luculent---Code----Generation----[接口单位:" + fullBussiName + "]------- 生成中。。。");

		CodeFactory codeFactory = new CodeFactory();
		codeFactory.setGenerator(new CodeGenerate());
		List<ExecuteModel> generateFiles = new ArrayList<ExecuteModel>();
		// 获取配置文件中的模板名称和生成的文件路径
		for (Map.Entry<String, ConfigValue> entry : path.getConfig("ONE").entrySet()) {
			ExecuteModel exe = new ExecuteModel(entry.getKey(), entry.getValue().unwrapped().toString(), false);
			generateFiles.add(exe);
		}
		for (Map.Entry<String, ConfigValue> entry : path.getConfig("TWO").entrySet()) {
			ExecuteModel exe = new ExecuteModel(entry.getKey(), entry.getValue().unwrapped().toString(), true);
			generateFiles.add(exe);
		}
		codeFactory.invoke(generateFiles);

		StaticLog.info("----luculent----Code----Generation-----[接口单位：" + fullBussiName + "]------ 生成完成。。。");
	}

	public static void main(String[] args) throws Exception {
		System.out.println("----luculent--------- Code------------- Generation -----[单位接口框架]------- 生成中。。。");
		new CodeGenerate().generateToFile();
		System.out.println("----luculent--------- Code------------- Generation -----[单位接口框架]------- 生成完成。。。");
	}

}
