package utils.genecode.generate.pathstrategy;

import org.apache.commons.lang.StringUtils;

import cn.hutool.core.util.StrUtil;
import utils.genecode.generate.ICodePathStyle;
import utils.genecode.util.CodeResourceUtil;
 
/**
 * 
 *@Description: 
 *@Author:ShiQiang
 *@Since:2017年5月18日下午3:15:57
 */

public class NormalStyle implements ICodePathStyle {

    @Override
    public String getPath(String path,String type,boolean change,String entityPackage,String entityName){
        StringBuilder str = new StringBuilder ();
        entityPackage = StringUtils.lowerCase (entityPackage);
        entityName = StringUtils.upperCase (entityName);
        if(change){
        	type = StrUtil.format(type, entityPackage,entityName);
        }else{
        	type = StrUtil.format(type,entityName);
        }
        str.append (path).append (CodeResourceUtil.CODEPATH);
        str.append(type);
        return str.toString();
    }

}
