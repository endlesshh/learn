package utils.genecode.generate.factory;

import utils.genecode.generate.BaseCodeFactory;
import utils.genecode.generate.ICodePathStyle;
import utils.genecode.generate.pathstrategy.NormalStyle;
import utils.genecode.util.CodeResourceUtil;
/**
 * 
 *@Description:生成文件路径的拼接
 *@Author:ShiQiang
 *@Since:2017年5月18日下午3:16:44
 */
public class CodeFactory extends BaseCodeFactory {

    private ICodePathStyle style = new NormalStyle ();

    public static String getProjectPath(){
        //String path = System.getProperty ("user.dir").replace ("\\", "/") + "/";
       // path = "E:/img/";
        String path = CodeResourceUtil.project_path;
        return path;
    } 

    public String getCodePath(String type,boolean change,String entityPackage,String entityName){
        return style.getPath (getProjectPath (), type,change, entityPackage, entityName);
    }

    public static void main(String[] args){

    }
}