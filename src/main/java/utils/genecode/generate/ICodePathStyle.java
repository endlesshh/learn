package utils.genecode.generate;

public interface ICodePathStyle {
	public String getPath(String path, String type,boolean change, String entityPackage, String entityName);
}
