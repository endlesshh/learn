package utils.genecode.constant;
/**
 * 
 *@Description:常亮
 *@Author:ShiQiang
 *@Since:2017年5月18日下午3:17:28
 */
public class Globals {

    /**  项目总名   */
    public static final String projectName             = "projectName"; 
    /**  单位名称   */
    public static final String bussiName            = "bussiName";
    /**  单位全称   */
    public static final String fullBussiName         = "fullBussiName";
    /**  描述         */
    public static final String ftlDescription         ="ftlDescription";
    /**  默认编码   */
    public static final String DefaultEncoding         = "UTF-8";
    
    public static final String ftlCreateTime         ="ftlCreateTime";
    
}
