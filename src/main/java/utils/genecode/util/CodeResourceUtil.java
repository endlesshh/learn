package utils.genecode.util;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

/**
 * 工具类，获取配置
 * @author 别人
 *
 */
public class CodeResourceUtil {

    private CodeResourceUtil() {}
   
    public static final Config bundlePath =  ConfigFactory.load("generatorcode/luculent_config.conf");
    /**
     * ----------项目相关默认配置--------------
     * */
    /**项目的路径*/
    public static String                project_path                =   bundlePath.getString("project_path");
    
    /**java 源码路径*/
    public static String                code_root_package         	=   bundlePath.getString("code_root_package").replace(".","/"); //"src/main/java";
    
    public static String                bussiPackage                =   bundlePath.getString("bussi_package"); //"luculent.fupin";
      
    public static String                bussiPackageUrl             = 	bussiPackage.replace(".", "/");//"luculent/fupin";
      
    public static String                TEMPLATEPATH                =	bundlePath.getString("templatepath");//"/generatorcode/template";
    /**代码路径*/
    public static String                CODEPATH                    = 	code_root_package + "/" + bussiPackageUrl + "/";
      /**编码格式*/
    public static String                SYSTEM_ENCODING 			= 	bundlePath.getString("system_encoding");

    public static String 				description					= 	bundlePath.getString("description");
    
    public static String 				danweifullname					= 	bundlePath.getString("danweifullname");
    
    public static String 				danweisimplename					= 	bundlePath.getString("danweisimplename");
     
}