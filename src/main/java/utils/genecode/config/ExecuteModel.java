package utils.genecode.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/***
 * 工厂方法根据该类的 模板名称和风格进行生成
 * @author ShiQiang
 *
 */
@Getter
@Setter
@AllArgsConstructor
public class ExecuteModel { 
    /**
     * 模板名称
     */
    private String templateName;
    /**
     * 类型名称如：jsp,service等
     */
    private String path; 
    
    private boolean change; 
   
}
