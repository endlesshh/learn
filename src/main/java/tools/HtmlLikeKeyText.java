package tools;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import cn.hutool.core.io.FileUtil;

public class HtmlLikeKeyText {
	
	static int count = 1;
	
	public static void main(String[] args) throws IOException {
		
		List<File> files = FileUtil.loopFiles("F:/szyz/中医宝典/zybd/中医书籍大全");
		for(File file : files){
			List<String> lines = FileUtil.readLines(file, "GBK");
			for(String doc : lines){
				if(hasKey(doc) == 1){
					Document document = Jsoup.parse(doc, "GBK");
					System.out.println(count++ +": " +document.text());
				}
			}
		}
		
	}
	public static int hasKey(String key){
		int result = 0;
		if(key.indexOf("嗳气") >= 0){
			result = 1;
		}
		 
		return result;
	}
} 