package tools;

import org.apache.commons.lang3.StringUtils;

public class EnCodeUtil{
	
	public static synchronized String next(String codeString,boolean join){
		if(StringUtils.isBlank(codeString)){
			return "0000";
		}
		if(join){
			return codeString = codeString + "_0000";
		} 
		String[] code = codeString.split("_");
    	String codeEnd = code[code.length-1];
    	codeEnd = createCode(codeEnd);
    	if("ZZZZ".equals(codeEnd)){
    		codeString = codeEnd + "_0000";
    	}else{
    		code[code.length-1] = codeEnd;
    		codeString = StringUtils.join(code, "_");
    		
    	}
    	return codeString;
	}
	
	public static String createCode(String idValue){
	    char[] chars=idValue.toCharArray();
	    if (chars[3]==57){
	        chars[3]+=8;
	    }else if (chars[3]<90){
	        chars[3]++;
	    } else {
	        if (chars[2]==57){
	            chars[2]+=8;
	            chars[3]=48;
	        }else if (chars[2]<90) {
	            chars[2]++;
	            chars[3] = 48;
	        }else {
	            if (chars[1]==57){
	                chars[1]+=8;
	                chars[2]=48;
	                chars[3]=48;
	            }else if (chars[1]<90){
	                chars[1]++;
	                chars[2]=48;
	                chars[3]=48;
	            }else {
	                if (chars[0]==57) {
	                    chars[0] += 8;
	                    chars[1] = 48;
	                    chars[2] = 48;
	                    chars[3] = 48;
	                }else if (chars[0]<90){
	                    chars[0] ++;
	                    chars[1] = 48;
	                    chars[2] = 48;
	                    chars[3] = 48;
	                }else {
	                    System.out.print("编码用完了");
	                }
	            }
	        }
	    }
	    StringBuffer sbu = new StringBuffer();
	    for (int i = 0; i < chars.length; i++) {
	        sbu.append(chars[i]);
	    }
	    return sbu.toString();
	}
	
	public static void main(String[] args){
	    String code = "";
	    while(true){
	    	code = next(code,false);
	    	System.out.println(code);
	    }
	}
}
