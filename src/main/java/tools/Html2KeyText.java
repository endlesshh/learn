package tools;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.common.collect.Lists;

import cn.hutool.core.io.FileUtil;

public class Html2KeyText {
	
	public static void main(String[] args) throws IOException {
		
		List<File> files = FileUtil.loopFiles("F:/szyz/中医宝典/zybd/中医书籍大全");
		List<Book> books = Lists.newArrayList();
		for (File file : files) {
			String name = file.getName();
			String pname = file.getParent();
			books.add(Book.builder().order(Integer.valueOf(name.substring(0, name.lastIndexOf("."))))
					.name(pname.substring(pname.lastIndexOf("\\") + 1, pname.length())).path(file.getAbsolutePath())
					.build());
		}
		books = books.stream().sorted(Comparator.comparing(Book::getOrder)).collect(Collectors.toList());
		Map<String, List<Book>> groups = books.stream().collect(Collectors.groupingBy(Book::getName));
		File file = new File("F:/zongyicd/key.txt");
		PrintWriter wirter = FileUtil.getPrintWriter(file, "GBK", true);
		for (String key : groups.keySet()) {
			
			List<Book> yema = groups.get(key);
			wirter.append(key);
			wirter.append("\n"); 
			for (Book bk : yema){
				if(bk.getOrder() == 0){
					Document document = Jsoup.parse(new File(bk.getPath()), "GBK");
					Elements title = document.select("ul[class=\"L0\"]").select("li");
					Iterator<Element> iter = title.iterator();
					while(iter.hasNext()){
						Element tt = iter.next();
						System.out.println(tt.text());
						wirter.append(tt.text());
						wirter.append("\n"); 
					} 
				} 
			} 
		}
		wirter.close();
	}
} 