package fuxingjue.tiyonghua;

import cn.hutool.log.StaticLog;
import fuxingjue.WuXing;

public class Tu extends WuXing{ 
	public Tu(){
		this.ti = "辛";
		this.yong = "甘";
		this.hua = "苦";
		this.name = "用土体";
	}

	@Override
	public WuXing jin() { 
		StaticLog.info("补脾");
		return new Jin();
	}

	@Override
	public WuXing tui() {
		StaticLog.info("泻脾");
		return new Huo();
	}
}
