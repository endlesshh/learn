package fuxingjue.tiyonghua;

import cn.hutool.log.StaticLog;
import fuxingjue.WuXing;

public class Shui extends WuXing{ 
	public Shui(){
		this.ti = "甘";
		this.yong = "苦";
		this.hua = "咸";
		this.name = "用水体";
	}

	@Override
	public WuXing jin() {
		StaticLog.info("补肾");
		return new Mu();
	}

	@Override
	public WuXing tui() {
		StaticLog.info("泻肾");
		return new Jin();
	}
}
