package fuxingjue.tiyonghua;

import cn.hutool.log.StaticLog;
import fuxingjue.WuXing;

public class Huo extends WuXing{ 
	public Huo(){
		this.ti = "苦";
		this.yong = "咸";
		this.hua = "酸";
		this.name = "用火体";
	}

	@Override
	public WuXing jin() { 
		StaticLog.info("补心");
		return new Tu();
	}

	@Override
	public WuXing tui() {
		StaticLog.info("泻心");
		return new Mu();
	} 
}
