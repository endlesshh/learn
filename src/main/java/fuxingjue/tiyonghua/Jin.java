package fuxingjue.tiyonghua;

import cn.hutool.log.StaticLog;
import fuxingjue.WuXing;

public class Jin extends WuXing{ 
	public Jin(){
		this.ti = "咸";
		this.yong = "酸";
		this.hua = "辛";
		this.name = "用金体";
	}

	@Override
	public WuXing jin() {
		StaticLog.info("补肺");
		return new Shui();
	}

	@Override
	public WuXing tui() {
		StaticLog.info("泻肺");
		return new Tu();
	}
}
