package fuxingjue.tiyonghua;

import cn.hutool.log.StaticLog;
import fuxingjue.WuXing;

public class Mu extends WuXing{ 
	public Mu(){
		this.ti = "酸";
		this.yong = "辛";
		this.hua = "甘";
		this.name = "用木体";
	}

	@Override
	public WuXing jin(){
		StaticLog.info("补肝");
		return new Huo();
	}

	@Override
	public WuXing tui() {
		StaticLog.info("泻肝");
		return new Shui();
	}
}
