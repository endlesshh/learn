package fuxingjue;

import java.lang.reflect.Field;

import cn.hutool.core.util.ReflectUtil;
import cn.hutool.log.StaticLog;

public abstract class WuXing { 
	
	public static int counts = 1;
	
	public String ti;
	
	public String yong;
	
	public String hua;
	
	public String name; 
	
	public WuXing bu(){
		Field[] fields = ReflectUtil.getFields(this.getClass());
		 
		for(Field fie :fields){
			System.out.println(fie.getName());
		}
		
		System.out.println(this);
		StaticLog.info("用味{}:{}-用-{}",counts,this.yong,SuoShu.get(this.yong).getName());
		counts++;
		return jin();
	}
	public WuXing xie(){
		StaticLog.info("体味{}:{}-用-{}",counts,this.ti,SuoShu.get(this.ti).getName());
		counts++;
		return tui();
	}
	public abstract WuXing jin();
	public abstract WuXing tui();
	
	
	
//	private WuXing() {} 
//    private static class SingletonInstance {
//        private static final WuXing INSTANCE = new WuXing();
//    } 
//    public static WuXing getInstance() {
//        return SingletonInstance.INSTANCE;
//    }
}
