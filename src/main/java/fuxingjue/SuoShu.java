package fuxingjue;


public enum SuoShu{
	
	jin("酸","金"), mu("辛","木"), shui("苦","水"), huo("咸","火"), tu("甘","土"); 
	
	private String wei;
	private String name;
	
	
	private SuoShu(String wei,String name){
		this.wei = wei;
		this.name = name;
	}
	
	public static SuoShu get(String wei){ 
		if(jin.wei.equals(wei)){
			return jin;
		}else if(mu.wei.equals(wei)){
			return mu;
		}else if(shui.wei.equals(wei)){
			return shui;
		}else if(huo.wei.equals(wei)){
			return huo;
		}else{
			return tu;
		} 
	} 
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}