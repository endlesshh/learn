package ik;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;

import cn.hutool.core.io.FileUtil;

public class IK {
	public static void main(String[] args) throws IOException { 
		List<String> lines = FileUtil.readLines("F:/zongyi/四圣心源.txt","GBK"); 
		for (String bk : lines) {  
			ik(bk);
		} 
	}
	
	public static void ik(String text) throws IOException{
		StringReader sr=new StringReader(text);
		IKSegmenter ik=new IKSegmenter(sr, true);
		Lexeme lex=null;
		while((lex=ik.next())!=null){
		    System.out.println(lex.getLexemeText()+"|");
		} 
	}
}
