package reactor.mytest;

import java.time.Duration;

import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;
import reactor.test.publisher.TestPublisher;

public class FluxStepVerifier {
	public static void main(String[] args) throws InterruptedException {
		
		//Hooks.onOperatorDebug();
		
		//Flux.just(1, 0).map(x -> 1 / x).checkpoint("test").subscribe(System.out::println);
		
		Flux.range(1, 2).log("Range").subscribe(System.out::println);
		
		StepVerifier.create(Flux.just("a", "b"))
        .expectNext("a")
        .expectNext("b")
        .verifyComplete();
		
		
		StepVerifier.withVirtualTime(() -> Flux.interval(Duration.ofHours(4), Duration.ofDays(1)).take(2))
        .expectSubscription()
        .expectNoEvent(Duration.ofHours(4))
        .expectNext(0L)
        .thenAwait(Duration.ofDays(1))
        .expectNext(1L)
        .verifyComplete();
		
		
		final TestPublisher<String> testPublisher = TestPublisher.create();
		testPublisher.next("a");
		testPublisher.next("b");
		testPublisher.complete();
		 
		StepVerifier.create(testPublisher)
		        .expectNext("a")
		        .expectNext("b")
		        .expectComplete();
		
		
		final Flux<Long> source = Flux.interval(Duration.ofMillis(1000))
		        .take(10)
		        .publish()
		        .autoConnect();
		source.subscribe();
		Thread.sleep(5000);
		source.toStream().forEach(System.out::println);
		
		
	}
}
