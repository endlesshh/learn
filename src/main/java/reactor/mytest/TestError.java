package reactor.mytest;

import java.util.function.Consumer;

import reactor.core.publisher.Flux;

public class TestError {
	public static void main(String[] args) {
		Flux.range(1, 10)
		.map(x -> {
			if(x == 3){
				throw new RuntimeException();
			}
			return Flux.just(1);})
		.doOnError(new Consumer<Throwable>(){

			@Override
			public void accept(Throwable e) {
				 e.printStackTrace();
			}}) 
		 
		.subscribe(System.out::println);
	}
}
