package reactor.mytest;

import java.time.Duration;
import java.util.function.Consumer;

import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

public class TestDelay {
	public static void main(String[] args) throws InterruptedException {
		Flux.range(1, 100)
		.delayElements(Duration.ofMillis(100))
		//.delaySubscription(Duration.ofMillis(100))
		//.take(Duration.ofNanos(984480l))
		//.subscribeOn(Schedulers.immediate())
		.subscribe(System.out::println);
		
		Thread.currentThread().sleep(100000l);
	}
}
