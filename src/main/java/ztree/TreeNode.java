package ztree;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TreeNode {

    /**
     * 结点编号
     */
    private String id;
    
    private String pId;

    /**
     * 结点名称
     */
    private String name;

    /**
     * 是否勾选
     */
    private boolean checked;

    /**
     * 是否禁止勾选
     */
    private boolean chkDisabled;

    /**
     * 是否半选
     */
    private boolean halfCheck;

    /**
     * 是否隐藏
     */
    private boolean hidden;

    /**
     * 是否是父结点
     */
    @JsonProperty("isParent")
    private boolean isParent;

    /**
     * 结点的子结点，如果是父结点，则存在子结点
     */
    private List<TreeNode> children;

    /**
     * 结点点击跳转的地址
     */
    private String url;

    /**
     * 结点点击跳转目标
     */
    private String target;

    /**
     * 结点上的应用数据
     */
    private Object data;

 
    private boolean notMy;

//    @OVERRIDE
//    PUBLIC STRING TOSTRING() {
//        RETURN "TREENODE{" +
//                "ID=" + ID +
//                ", CHECKED=" + CHECKED +
//                ", CHKDISABLED=" + CHKDISABLED +
//                ", HALFCHECK=" + HALFCHECK +
//                ", HIDDEN=" + HIDDEN +
//                ", PARENT=" + PARENT +
//                ", NAME='" + NAME + '\'' +
//                ", URL='" + URL + '\'' +
//                ", TARGET='" + TARGET + '\'' +
//                ", DATA=" + DATA +
//                '}';
//    }

  


    public static void main(String[] args) {
        TreeNode treeNode = TreeNode.builder().id("2").name("jsck").build();
        System.out.println(treeNode);

//        Gson gson = new Gson();
//        System.out.println(gson.toJson(treeNode));
    }
}