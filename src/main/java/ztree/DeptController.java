package ztree;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <pre>
 * 部门管理
 * </pre>
 * 
 * <small> 2018年3月23日 | Aron</small>
 */
@Controller
@RequestMapping("/sys/dept")
public class DeptController  {
    private String prefix = "sys/dept"; 
 
    /**
     * 初始化使用
     * @return
     */
//    @PostMapping("/zTree")
//    @ResponseBody 
//    public List<TreeNode> zTree(){
//       return sysDeptService.getDeptByAuth(ShiroUtils.getSysUser().getDeptId());
//    }
//    /**
//     * 根据id查询
//     * @param id
//     * @param name
//     * @param level
//     * @return
//     */
//    @PostMapping("/zTreeByPid")
//    @ResponseBody
//    @Log("查询部门树形数据")
//    public List<TreeNode> zTreesByPid(String id,String name,String level){
//        return sysDeptService.getZTree(id);
//    }
//
//    @GetMapping("/treeView")
//    @Log("进入部门树形显示页面")
//    String treeView( Model model) {
//    	model.addAttribute("deptId", ShiroUtils.getSysUser().getDeptId());
//        return prefix + "/dept_and_user";
//    }
//
//    @PostMapping("/zTreeEditNameById")
//    @ResponseBody
//    @Log("查询部门树形数据")
//    public Result<String> editDeptByZTree(String id,String name){
//    	if(CheckUtil.isNull(id,name)){
//    		return Result.fail();
//    	}
//    	DeptDO dept = sysDeptService.selectById(id);
//    	if(dept == null ){
//    		return Result.fail();
//    	}
//    	dept.setName(name);
//    	sysDeptService.updateById(dept);
//        return Result.ok();
//    }

}
